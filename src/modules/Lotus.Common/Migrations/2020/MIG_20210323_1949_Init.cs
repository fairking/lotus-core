using FluentMigrator;

namespace Lotus.Core.Data.Migrations
{
    [Migration(20210323_1949)]
    public class MIG_20210323_1949_Init : AutoReversingMigration
    {
        public override void Up()
        {
            #region Users

            {
                var usersTable = Create.Table("com_users");

                usersTable.WithColumn("id").AsAnsiString(16).PrimaryKey("pk_com_users");
                usersTable.WithColumn("created_by").AsString(50).Nullable();
                usersTable.WithColumn("updated_by").AsString(50).Nullable();
                usersTable.WithColumn("created_date").AsDateTime().WithDefault(SystemMethods.CurrentUTCDateTime);
                usersTable.WithColumn("updated_date").AsDateTime().WithDefault(SystemMethods.CurrentUTCDateTime);
                usersTable.WithColumn("version").AsInt32().WithDefaultValue(1);
                usersTable.WithColumn("is_deleted").AsBoolean().NotNullable().WithDefaultValue(false).Indexed("ix_com_users_is_deleted");

                usersTable.WithColumn("username").AsString(50).NotNullable().Unique("uq_com_users_username").Indexed("ix_com_users_username");
                usersTable.WithColumn("email").AsString(150).Nullable();
                usersTable.WithColumn("password").AsString(150).Nullable();
                usersTable.WithColumn("role").AsString(50).NotNullable().Indexed("ix_com_users_role");
            }

            #endregion Users

        }
    }
}
