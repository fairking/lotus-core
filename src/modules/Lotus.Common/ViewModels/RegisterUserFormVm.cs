﻿using Lotus.Core.Validation;
using Lotus.Core.ViewModels;

namespace Lotus.Common.ViewModels
{
    public class RegisterFormVm : BaseCreateFormVm
    {
        [ValRegex("^[a-z0-9_]+$")]
        [ValRequired]
        [ValLength(50, min: 3)]
        public string Username { get; set; }

        [ValEmail]
        public string Email { get; set; }

        [ValRequired]
        [ValLength(64, min: 8)]
        public string Password { get; set; }
    }
}
