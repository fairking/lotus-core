﻿using Lotus.Common.Entities;
using Lotus.Common.ViewModels;
using Lotus.Core.Routing;
using Lotus.Core.Services;
using Lotus.Helpers;
using System;
using System.Threading.Tasks;

namespace Lotus.Common.Services
{
    public class UserService : BaseService
    {
        public UserService(IServiceProvider services) : base(services)
        {

        }

        [Route("*", Policies = "*", Transactional = true)]
        public async Task<string> Register(RegisterFormVm model)
        {
            var user = new User(model.Username, User.UserRoles.User);

            user.SetPassword(GetPasswordHash(model.Username, model.Password, Config.Security.SecuritySalt));

            user.SetEmail(model.Email);

            var userId = await SaveOrUpdateAsync(user);

            return userId;
        }

        // Create User

        // Edit User

        // Delete User

        // Register

        // Edit Profile

        // Remind Password

        // Deactivate Profile

        // Delete Profile

        private string GetPasswordHash(string username, string password, string salt)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            if (string.IsNullOrWhiteSpace(salt))
                throw new ArgumentNullException(nameof(salt));

            var join = username + password + salt;

            var result = join.ToSha512Hash();

            return result;
        }
    }
}
