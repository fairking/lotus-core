﻿using Lotus.Core.Events;
using Lotus.Core.Routing;
using Lotus.Core.Services;
using Lotus.Core.ViewModels;
using Lotus.Msm.Events;
using Lotus.Msm.Services;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Lotus.Common.Services
{
    public class MyService : BaseService
    {
        public MyService(IServiceProvider services): base(services)
        {

        }

        [Route("*", Policies = "*", Transactional = true)]
        public async Task<string> MyMethod(IdVm model)
        {
            GetService<SystemEventService>().AddEventToQueue(new MyEvent() { MyInfo = "ABCEV" });
            return "ABC " + model.Id;
        }

        [MyEvent]
        public async Task MyMethod2(MyEvent e, CancellationToken cancellationToken)
        {
            Debug.WriteLine("Event happened: " + e.MyInfo);
        }

        [ApplicationStartupEvent(EventTypeEnum.AsyncBackground)]
        public async Task MyMethod3()
        {
            Debug.WriteLine("Startup happened: ");
        }

        [Cron("*/1 * * * *")]
        public async Task MyMethod4()
        {
            Debug.WriteLine("Cron happened: ");
        }
    }
}
