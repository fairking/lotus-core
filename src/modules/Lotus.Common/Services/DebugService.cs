﻿using Lotus.Core.Exceptions;
using Lotus.Core.Routing;
using Lotus.Core.Services;
using System;
using System.Threading.Tasks;

namespace Lotus.Common.Services
{
    public class DebugService : BaseService
    {
        public DebugService(IServiceProvider services) : base(services)
        {
            if (!GetService<CoreConfigService>().Settings.Debug)
                throw new UserException("Debug mode is disabled.");
        }

        [Route("*", Policies = "*", AllowAnonymous = true)]
        public async Task ThrowError()
        {
            throw new Exception("This is dummy error.");
        }

        [Route("*", Policies = "*", AllowAnonymous = true)]
        public async Task ThrowUserError()
        {
            throw new UserException("This is dummy user error.");
        }
    }
}
