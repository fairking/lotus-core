﻿using Lotus.Abstract;
using Microsoft.Extensions.Configuration;
using System;

namespace Lotus.Common.Services
{
    public class CommonConfigService : IService
    {
        private IConfiguration _config;

        public CommonConfigService(IConfiguration config)
        {
            _config = config?.GetSection("Common") ?? throw new ArgumentNullException(nameof(config));
        }

        private AuthSettings _auth;

        public virtual AuthSettings Auth
        {
            get
            {
                if (_auth == null)
                {
                    _auth = new AuthSettings();
                    _config.GetSection(nameof(Auth)).Bind(_auth);

                    if (_auth.NewPasswordGeneratedLength <= 0)
                        _auth.NewPasswordGeneratedLength = 8;

                    if (_auth.NewPasswordGeneratedLength < 8)
                        throw new Exception($"Cannot set password length less than 8. (See appconfig.json -> {nameof(Auth)} -> {nameof(AuthSettings.NewPasswordGeneratedLength)}).");
                }
                return _auth;
            }
        }

    }

    public class AuthSettings
    {
        /// <summary>
        /// By default the JWT token is stored in cookies without client access. If the browser does not support secure cookies this option must be disabled (JWT will be stored in browser storage or based on the client app logic).
        /// </summary>
        public bool DisableSecureCookies { get; set; }

        public int AuthTokenExpiresMinutes { get; set; }

        public int LogoutAfterInactivityMinutes { get; set; }

        public int NewPasswordGeneratedLength { get; set; }
    }

}
