﻿using Lotus.Core.Entities;
using Lotus.Sql.Attributes;
using System;

namespace Lotus.Common.Entities
{
    [Table("com_users")]
    public class User : BaseEntity
    {
        protected User() { }

        public User(string username, UserRoles role)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException();

            Email = username;
            Role = role;
        }

        public void SetPassword(string password)
        {
            Password = password;
        }

        public void SetEmail(string email)
        {
            Email = email;
        }

        public virtual string Username { get; protected set; }

        public virtual string Email { get; protected set; }

        public virtual string Password { get; protected set; }

        public virtual UserRoles Role { get; protected set; }

        public enum UserRoles
        {
            User,
            Admin,
        }
    }
}
