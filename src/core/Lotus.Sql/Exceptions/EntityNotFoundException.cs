﻿using System;

namespace Lotus.Sql.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(Type entityType, string id) : base($"Entity '{entityType.Name}' with Id '{id}' not found.")
        {

        }
    }
}
