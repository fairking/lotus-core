﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Lotus.Sql.Services
{
    public class PostgresBackupService
    {
        public void PostgreSqlDump(
            string pgDumpPath,
            string outFile,
            string host,
            string port,
            string database,
            string user,
            string password)
        {
            var dumpCommand = "\"" + pgDumpPath + "\"" + " -Fc" + " -h " + host + " -p " + port + " -d " + database + " -U " + user + "";
            var passFileContent = "" + host + ":" + port + ":" + database + ":" + user + ":" + password + "";

            var batFilePath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".bat");

            var passFilePath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".conf");

            try
            {
                var batchContent = "";
                batchContent += "@" + "set PGPASSFILE=" + passFilePath + "\n";
                batchContent += "@" + dumpCommand + "  > " + "\"" + outFile + "\"" + "\n";

                File.WriteAllText(batFilePath, batchContent, Encoding.ASCII);

                File.WriteAllText(passFilePath, passFileContent, Encoding.ASCII);

                if (File.Exists(outFile))
                    File.Delete(outFile);

                var oInfo = new ProcessStartInfo(batFilePath)
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                };

                using (var proc = Process.Start(oInfo))
                {
                    proc.WaitForExit();
                    proc.Close();
                }
            }
            finally
            {
                if (File.Exists(batFilePath))
                    File.Delete(batFilePath);

                if (File.Exists(passFilePath))
                    File.Delete(passFilePath);
            }
        }
    }
}
