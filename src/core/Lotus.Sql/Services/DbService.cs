﻿using Lotus.Sql.Attributes;
using CaseExtensions;
using Microsoft.Data.Sqlite;
using Npgsql;
using SqlKata;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Data;
using System.Reflection;

namespace Lotus.Sql.Services
{
    public class DbService : IDisposable
    {
        private QueryFactory _queryFactory;

        private IDbTransaction _transaction;

        public DbService(string connectionString, DatabaseType dbType, int timeout = 30)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            IDbConnection connection;
            Compiler compiler;

            switch (dbType)
            {
                case DatabaseType.sqlite:
                    connection = new SqliteConnection(connectionString);
                    compiler = new SqliteCompiler();
                    break;
                case DatabaseType.postgresql:
                    connection = new NpgsqlConnection(connectionString);
                    compiler = new SqliteCompiler();
                    break;
                default:
                    throw new NotImplementedException($"DbType '{dbType}' is not implemented.");
            }

            _queryFactory = new QueryFactory(connection, compiler, timeout);
        }

        public IDbConnection Connection => _queryFactory.Connection;

        public void BeginTransaction()
        {
            if (_transaction != null)
                throw new Exception("Database transaction already exists.");

            if (Connection.State != ConnectionState.Open)
                Connection.Open();

            _transaction = Connection.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void Commit()
        {
            if (_transaction == null)
                throw new Exception("Database transaction does not exists.");

            _transaction.Commit();
            _transaction.Dispose();
            _transaction = null;
        }

        public void Rollback()
        {
            if (_transaction == null)
                throw new Exception("Database transaction does not exists.");

            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;
        }

        public Query Query()
        {
            return _queryFactory.Query();
        }

        public Query Query<T>() where T : class
        {
            var table = typeof(T).GetCustomAttribute<TableAttribute>()?.Name;

            if (string.IsNullOrEmpty(table))
                table = typeof(T).Name.ToSnakeCase();

            return _queryFactory.Query(table);
        }

        public void Dispose()
        {
            if (_transaction != null)
                Rollback();

            if (Connection.State == ConnectionState.Open)
                Connection.Close();

            Connection.Dispose();

            _queryFactory.Dispose();
        }

        public enum DatabaseType
        {
            sqlite,
            postgresql,
        }
    }
}
