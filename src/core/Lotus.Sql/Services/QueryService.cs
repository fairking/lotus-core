﻿using Lotus.Abstract;
using Lotus.Helpers;
using Lotus.Sql.Exceptions;
using Lotus.Sql.Observable;
using CaseExtensions;
using SqlKata;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Lotus.Sql.Services
{
    public class QueryService : IDisposable
    {
        private readonly DbService _db;
        private readonly IUserContext _userContext;
        private readonly IDateTime _dateTime;

        public QueryService(DbService db, IUserContext userContext, IDateTime dateTime)
        {
            _db = db;
            _userContext = userContext;
            _dateTime = dateTime;
        }

        public async Task<T> GetAsync<T>(string id) where T : class, IEntity, new()
        {
            var result = (await GetOrDefaultAsync<T>(id))
                ?? throw new EntityNotFoundException(typeof(T), id);

            return result;
        }

        public async Task<T> GetOrDefaultAsync<T>(string id) where T : class, IEntity, new()
        {
            var result = await _db.Query<T>().Where(nameof(IEntity).ToCamelCase(), id).GetAsync<T>();
            return ObservableProxy.Create(result.SingleOrDefault());
        }

        public async Task<string> SaveOrUpdateAsync<T>(T obj) where T : class
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var id = obj as IHaveId;

            if (id == null)
                throw new ArgumentException("Provided object must have an Id.");

            bool changed = true;

            bool proxy = ObservableProxy.IsProxy(obj);

            if (proxy)
            {
                changed = ObservableProxy.IsChanged(obj);
            }

            if (changed)
            {
                if (string.IsNullOrWhiteSpace(id.Id))
                {
                    // Insert
                    GenerateId(obj);
                    ApplyAudit(obj, true);
                    await _db.Query<T>().InsertAsync(GetColumns(obj));
                }
                else
                {
                    // Update
                    ApplyAudit(obj, false);
                    await _db.Query<T>().UpdateAsync(GetColumns(obj, changedOnly: true));
                }
            }

            if (proxy && changed)
            {
                ObservableProxy.ClearChanged(obj);
            }

            return id.Id;
        }

        #region Private Methods

        private IDictionary<string, object> GetColumns<T>(T obj, bool changedOnly = false) where T : class
        {
            var columns = new Dictionary<string, object>();

            IEnumerable<PropertyInfo> properties;

            if (changedOnly && ObservableProxy.IsProxy(obj))
            {
                var changedProps = ObservableProxy.ChangedProperties(obj);
                properties = typeof(T).GetProperties().Where(x => changedProps.Contains(x.Name));
            }
            else
            {
                properties = typeof(T).GetProperties();
            }

            foreach (var prop in properties)
            {
                if (prop.GetCustomAttribute<IgnoreAttribute>() != null)
                    continue;

                var columnName = prop.GetCustomAttribute<ColumnAttribute>()?.Name ?? prop.Name.ToSnakeCase();

                columns.Add(columnName, prop.GetValue(obj));
            }

            return columns;
        }

        private void GenerateId<T>(T obj) where T : class
        {
            var id = obj as IHaveId;
            typeof(T).GetProperty(nameof(IHaveId.Id)).SetValue(obj, IdentityGenerator.WebHash());
        }

        private void ApplyAudit<T>(T obj, bool insert) where T : class
        {
            var audit = obj as IAudit;

            if (audit == null)
                return;

            var now = _dateTime.GetNowUtc();

            if (insert)
            {
                typeof(T).GetProperty(nameof(IAudit.UpdatedBy)).SetValue(obj, _userContext.UserId);
                typeof(T).GetProperty(nameof(IAudit.UpdatedTime)).SetValue(obj, now);
            }

            typeof(T).GetProperty(nameof(IAudit.UpdatedBy)).SetValue(obj, _userContext.UserId);
            typeof(T).GetProperty(nameof(IAudit.UpdatedTime)).SetValue(obj, now);

            typeof(T).GetProperty(nameof(IAudit.Version)).SetValue(obj, audit.Version + 1);
        }

        #endregion Private Methods

        public void Dispose()
        {

        }
    }
}
