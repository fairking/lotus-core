﻿using Castle.DynamicProxy;
using System;
using System.Collections;
using System.Linq;

namespace Lotus.Sql.Observable
{
    public class ObservableProxy
    {
        private static readonly ProxyGenerator Generator = new ProxyGenerator();

        public static T Create<T>() where T : class, new()
        {
            return Generator.CreateClassProxy<T>(new ObservableInterceptor());
        }

        public static T Create<T>(T obj) where T : class
        {
            return Generator.CreateClassProxyWithTarget(obj, new ObservableInterceptor());
        }

        public static bool IsProxy<T>(T obj) where T : class
        {
            return ProxyUtil.IsProxy(obj);
        }

        public static T Unproxy<T>(T obj) where T : class
        {
            if (!ProxyUtil.IsProxy(obj))
                return obj;

            const System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
            var proxyType = obj.GetType();
            var targetField = proxyType.GetField("__target", flags);
            var target = targetField.GetValue(obj);
            return (T)target;
        }

        public static bool IsChanged<T>(T obj) where T : class
        {
            if (!IsProxy(obj))
                throw new ArgumentException("Cannot check changes because the object is not a proxy.");

            var interceptor = GetInterceptor(obj)
                ?? throw new ArgumentException("Cannot check changes because the proxy has no ObservableInterceptor implemented.");

            return interceptor.IsChanged;
        }

        public static string[] ChangedProperties<T>(T obj) where T : class
        {
            if (!IsProxy(obj))
                throw new ArgumentException("Cannot check changes because the object is not a proxy.");

            var interceptor = GetInterceptor(obj)
                ?? throw new ArgumentException("Cannot check changes because the proxy has no ObservableInterceptor implemented.");

            return interceptor.ChangedProperties.ToArray();
        }

        public static void ClearChanged<T>(T obj) where T : class
        {
            if (!IsProxy(obj))
                throw new ArgumentException("Cannot check changes because the object is not a proxy.");

            var interceptor = GetInterceptor(obj)
                ?? throw new ArgumentException("Cannot check changes because the proxy has no ObservableInterceptor implemented.");

            interceptor.ClearChanges();
        }

        private static ObservableInterceptor GetInterceptor<T>(T obj) where T : class
        {
            const System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
            var proxyType = obj.GetType();
            var interceptorsField = proxyType.GetField("__interceptors", flags);
            var interceptor = (interceptorsField.GetValue(obj) as IEnumerable).OfType<ObservableInterceptor>().SingleOrDefault();
            if (interceptor == null)
                throw new ArgumentException("Cannot check changes because the proxy has no ObservableInterceptor implemented.");
            return interceptor;
        }
    }
}
