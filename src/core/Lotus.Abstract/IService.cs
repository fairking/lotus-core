﻿namespace Lotus.Abstract
{
    /// <summary>
    /// Singleton Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// </summary>
    public interface IService
    {

    }

    /// <summary>
    /// Singleton Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// T is an interface to register the service with
    /// </summary>
    public interface IService<T> : IService where T : class
    {

    }

    /// <summary>
    /// Scoped Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// </summary>
    public interface IScopedService : IService
    {

    }

    /// <summary>
    /// Scoped Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// T is an interface to register the service with
    /// </summary>
    public interface IScopedService<T> : IScopedService, IService<T> where T : class
    {

    }

    /// <summary>
    /// Transient Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// </summary>
    public interface ITransientService : IService
    {

    }

    /// <summary>
    /// Transient Service. Put this interface against services you want to setup them automatically by ScanAndRegisterAllServices. Otherwise you have to setup them manually.
    /// T is an interface to register the service with
    /// </summary>
    public interface ITransientService<T> : ITransientService, IService<T> where T : class
    {

    }

}
