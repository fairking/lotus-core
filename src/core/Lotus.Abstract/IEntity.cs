﻿namespace Lotus.Abstract
{
    public interface IEntity : IHaveId, IAudit, IDeletable
    {
    }
}
