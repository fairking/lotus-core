﻿namespace Lotus.Abstract
{
    public interface IUser : IHaveId
    {
        string TimeZone { get; }
        string Culture { get; }
    }
}
