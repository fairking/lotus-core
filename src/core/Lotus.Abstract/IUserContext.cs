﻿namespace Lotus.Abstract
{
    public interface IUserContext
    {
        string UserId { get; }
        string TimeZone { get; }
        string Culture { get; }

        void SetUser(IUser user);

        void SetUser(string userId, string timezone, string culture);
    }
}
