﻿using System;

namespace Lotus.Abstract
{
    public interface IAudit : IHaveId
    {
        public string CreatedBy { get; }

        public string UpdatedBy { get; }

        public DateTime CreatedTime { get; }

        public DateTime UpdatedTime { get; }

        public int Version { get; }
    }
}
