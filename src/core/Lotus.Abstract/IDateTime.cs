﻿using System;

namespace Lotus.Abstract
{
    public interface IDateTime
    {
        public DateTime GetNowUtc();
    }
}
