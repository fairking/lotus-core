﻿namespace Lotus.Abstract
{
    public interface IDeletable
    {
        public bool IsDeleted { get; }
    }
}
