﻿namespace Lotus.Abstract
{
    public interface IHaveId
    {
        string Id { get; }
    }
}
