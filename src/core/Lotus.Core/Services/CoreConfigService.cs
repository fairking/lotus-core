﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Lotus.Core.Services
{
    public class CoreConfigService
    {
        private IConfiguration _config;
        public static string AppVersion;
        public static bool IsDevelopment;
        public const string DevEnvName = "Development";

        [Obsolete("For mocking only")]
        protected CoreConfigService()
        {
        }

        public CoreConfigService(IConfiguration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        private DbSettings _database;

        public virtual DbSettings Database
        {
            get
            {
                if (_database == null)
                {
                    _database = new DbSettings();
                    GetSection(nameof(Database)).Bind(_database);

                    if (string.IsNullOrWhiteSpace(_database.Connection))
                        throw new Exception($"Database connection is required. (See appconfig.json -> {nameof(Database)} -> {nameof(DbSettings.Connection)}).");
                }
                return _database;
            }
        }

        private AppSettings _settings;

        public virtual AppSettings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new AppSettings();
                    GetSection(nameof(Settings)).Bind(_settings);

                    if (string.IsNullOrWhiteSpace(_settings.AppName))
                        _settings.AppName = "Lotus";

                    if (string.IsNullOrWhiteSpace(_settings.AppInstanceName))
                        _settings.AppInstanceName = "lotus";

                    if (_settings.Os == null)
                        throw new Exception($"Operational system is required. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.Os)}).");

                    if (!Regex.IsMatch(_settings.AppInstanceName, @"^[A-Za-z][A-Za-z\d_]+$"))
                        throw new Exception($"Aplication instance name must contain only lower alphabetical characters, numbers and underscore. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.AppInstanceName)}).");

                    if (string.IsNullOrWhiteSpace(_settings.CompanyName))
                        throw new Exception($"Company name is required. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.CompanyName)}).");

                    if (string.IsNullOrWhiteSpace(_settings.AppServerUrl))
                        throw new Exception($"Server Url is required. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.AppServerUrl)}).");

                    if (string.IsNullOrWhiteSpace(_settings.AppClientUrl))
                        throw new Exception($"Client Url is required. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.AppClientUrl)}).");

                    if (string.IsNullOrWhiteSpace(_settings.FilesPath))
                        throw new Exception($"Attachments path is required. (See appconfig.json -> {nameof(Settings)} -> {nameof(AppSettings.FilesPath)}).");

                    if (_settings.Modules == null)
                        _settings.Modules = new string[0];
                }
                return _settings;
            }
        }

        private SecuritySettings _security;

        public virtual SecuritySettings Security
        {
            get
            {
                if (_security == null)
                {
                    _security = new SecuritySettings();
                    GetSection(nameof(Security)).Bind(_security);

                    if (string.IsNullOrWhiteSpace(_security.SecuritySalt))
                        throw new Exception($"Security salt is required. (See appconfig.json ->  {nameof(Security)} -> {nameof(SecuritySettings.SecuritySalt)}).");
                }
                return _security;
            }
        }

        private JobsSettings _jobs;

        public virtual JobsSettings Jobs
        {
            get
            {
                if (_jobs == null)
                {
                    _jobs = new JobsSettings();
                    GetSection(nameof(Jobs)).Bind(_jobs);

                    if (string.IsNullOrWhiteSpace(_jobs.DbConnection))
                        _jobs.DbConnection = Database.Connection;

                    if (_jobs.ExcludeRecurringTasks == null)
                        _jobs.ExcludeRecurringTasks = new string[0];
                }
                return _jobs;
            }
        }

        private List<AdminAccount> _initialAdmins;

        public virtual IReadOnlyList<AdminAccount> InitialAdmins
        {
            get
            {
                if (_initialAdmins == null)
                {
                    _initialAdmins = new List<AdminAccount>();
                    GetSection(nameof(InitialAdmins)).Bind(_initialAdmins);
                }
                return _initialAdmins;
            }
        }

        private SmtpSettings _smtp;

        public virtual SmtpSettings Smtp
        {
            get
            {
                if (_smtp == null)
                {
                    _smtp = new SmtpSettings();
                    GetSection(nameof(Smtp)).Bind(_smtp);
                }
                return _smtp;
            }
        }

        private BackupSettings _backup;

        public virtual BackupSettings Backup
        {
            get
            {
                if (_backup == null)
                {
                    _backup = new BackupSettings();
                    GetSection(nameof(Backup)).Bind(_backup);
                }
                return _backup;
            }
        }

        private LoggingSettings _logging;

        public virtual LoggingSettings Logging
        {
            get
            {
                if (_logging == null)
                {
                    _logging = new LoggingSettings();
                    GetSection(nameof(Logging)).Bind(_logging);
                    if (string.IsNullOrWhiteSpace(_logging.FileDirectory))
                        _logging.FileDirectory = Directory.GetCurrentDirectory() + "\\Logs";
                }
                return _logging;
            }
        }

        protected IConfiguration GetSection(string sectionName)
        {
            if (string.IsNullOrWhiteSpace(sectionName))
                throw new ArgumentNullException(nameof(sectionName));

            return _config.GetSection(sectionName);
        }

        #region Types

        public class AdminAccount
        {
            public string DisplayName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public enum OsEnum
        {
            windows,
            linux,
        }

        public class AppSettings
        {
            /// <summary>
            /// Debug mode (Allows to use DebugService and adds additional logging)
            /// </summary>
            public bool Debug { get; set; }

            /// <summary>
            /// Operational system where the application is installed. Available options: windows, linux
            /// </summary>
            public OsEnum? Os { get; set; }

            /// <summary>
            /// Name of application shown in title
            /// </summary>
            public string AppName { get; set; } = "Lotus";

            /// <summary>
            /// Company name shown in application header.
            /// </summary>
            public string CompanyName { get; set; }

            /// <summary>
            /// API Url
            /// </summary>
            public string AppServerUrl { get; set; } = "http://localhost";

            /// <summary>
            /// Client Url
            /// </summary>
            public string AppClientUrl { get; set; } = "http://localhost";

            /// <summary>
            /// Url service which allows to generate the users's avatars (eg. "https://robohash.org/{0}.png?set=set4")
            /// </summary>
            public string AvatarServiceUrl { get; set; }

            /// <summary>
            /// In case you have many instances on one server name them differently (eg. "lotus_customer1", "lotus_customer2").
            /// </summary>
            public string AppInstanceName { get; set; } = "lotus";

            /// <summary>
            /// Email Address for sending emails to the client's administrator
            /// </summary>
            public string AdminEmail { get; set; }

            /// <summary>
            /// Folder with all lotus files. Please configure every instance with its own folder.
            /// </summary>
            public string FilesPath { get; set; }

            /// <summary>
            /// Application modules. You can change them in production only.
            /// </summary>
            public string[] Modules { get; set; }
        }

        public class DbSettings
        {
            /// <summary>
            /// Database connection string. Uses DefaultConnection if empty. (See https://www.connectionstrings.com/)
            /// </summary>
            public string Connection { get; set; }

            /// <summary>
            /// Database type. Available options: mssql | mssqlazure | postgresql | oracle
            /// </summary>
            public Sql.Services.DbService.DatabaseType Type { get; set; }
        }

        public class SecuritySettings
        {
            /// <summary>
            /// Salt is random generated string used for encoding passwords and other stuff. Every client must have
            /// their own different salt. The salt cannot be changed later otherwise users will lost their passwords and
            /// access to the application.
            /// </summary>
            public string SecuritySalt { get; set; }

            /// <summary>
            /// Shows all api methods which require authorization in http://maximus_url/api/index.html
            /// </summary>
            public bool EnableApiScheme { get; set; }
        }

        public class JobsSettings
        {
            /// <summary>
            /// Jobs database connection. Leave empty if you want to use default connection.
            /// </summary>
            public string DbConnection { get; set; }

            /// <summary>
            /// List of Recurring tasks (see CronAttribute)
            /// </summary>
            public string[] ExcludeRecurringTasks { get; set; }

            /// <summary>
            /// Number of treads for main queue
            /// </summary>
            public int MainWorkerCount { get; set; }

            /// <summary>
            /// Number of treads for background queue
            /// </summary>
            public int BackgroundWorkerCount { get; set; }
        }

        public class BackupSettings
        {
            /// <summary>
            /// Empty string will disable backups. Use cron syntax described here https://en.wikipedia.org/wiki/Cron#CRON_expression
            /// </summary>
            public string Cron { get; set; }

            /// <summary>
            /// Empty string will disable backups
            /// </summary>
            public string FilesPath { get; set; }

            /// <summary>
            /// Keeps number of recent files and removes anything else from the backup folder. 0 means no limitations
            /// </summary>
            public int KeepFilesCount { get; set; }

            /// <summary>
            /// Include database backup. Only availabe for mssql databases at the moment
            /// </summary>
            public bool IncludeDatabase { get; set; }

            /// <summary>
            /// Include attachments backup.
            /// </summary>
            public bool IncludeAttachments { get; set; }
        }

        public class SmtpSettings
        {
            public string Host { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public int Port { get; set; }
            public bool UseSsl { get; set; }
            public string DisplayName { get; set; }
            public string EmailAddress { get; set; }
            public string DefaultReplyAddress { get; set; }
            public string SaveEmailsToDiskPath { get; set; }
        }

		public class LoggingSettings
		{
			public string Console { get; set; }

			public string File { get; set; }

			public string FileDirectory { get; set; }

			public string Mail { get; set; }

			public string LogsEmail { get; set; }
		}

		#endregion Types

	}
}
