﻿using Lotus.Abstract;
using Lotus.Core.Exceptions;
using Lotus.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Lotus.Core.Services
{
    public class ServiceExecuter
    {
        public readonly IReadOnlyDictionary<string, Type> Services;

        public ServiceExecuter(Assembly[] assemblies)
        {
            Services = new ReadOnlyDictionary<string, Type>(GetServicesInner(assemblies));
        }

        public Type GetServiceType(string serviceName)
        {
            var serviceType = Services.GetValueOrDefault(TrimServiceName(serviceName));
            if (serviceType == null)
                throw new Exception($"The service type '{serviceName}' not found.");

            return serviceType;
        }

        public async Task<object> ExecuteService<TAttr>(string service, string operation, IServiceProvider serviceProvider, ControllerBase controller, CancellationToken token = default) where TAttr : Attribute
        {
            // get service type
            var serviceType = GetServiceType(service);

            // get the service method
            var serviceMethod = GetServiceMethod(serviceType, operation, typeof(TAttr));

            // get the instance
            var serviceInstance = serviceProvider.GetService(serviceType);
            if (serviceInstance == null)
                throw new Exception($"The service '{service}' not found.");

            // get parameters instances
            var parameters = serviceMethod.GetParameters().ToList();
            var arguments = new Dictionary<string, object>();
            var serviceBaseType = typeof(IService);
            foreach (var p in parameters)
            {
                if (serviceBaseType.IsAssignableFrom(p.ParameterType)) // Services
                {
                    var model = serviceProvider.GetService(p.ParameterType);
                    arguments.Add(p.Name, model);
                }
                else if (!p.ParameterType.IsValueType && Nullable.GetUnderlyingType(p.ParameterType) == null) // Objects
                {
                    var model = Activator.CreateInstance(p.ParameterType);
                    await controller.TryUpdateModelAsync(model, p.ParameterType, "");
                    var validationResults = new List<ValidationResult>();
                    var isValid = true;
                    if (model is IEnumerable)
                    {
                        foreach (var item in model as IEnumerable)
                        {
                            isValid = isValid && Validator.TryValidateObject(item, new ValidationContext(item, serviceProvider: null, items: null), validationResults, true);
                        }
                    }
                    else
                    {
                        isValid = Validator.TryValidateObject(model, new ValidationContext(model, serviceProvider: null, items: null), validationResults, true);
                    }
                    if (!isValid)
                    {
                        if (validationResults.Any())
                        {
                            throw new UserException(validationResults.Select(x => x.MemberNames.FirstOrDefault()).ToArray(), validationResults.Select(x => x.ErrorMessage).ToArray());
                        }
                        else
                        {
                            throw new UserException("Error. Input data are incorrect.");
                        }
                    }
                    arguments.Add(p.Name, model);
                }
                else // Struct or Nullable<>
                {
                    // TODO: Need more work around single value parameters.
                    // Currently they are not supported
                    throw new NotImplementedException();
                }
            }

            var result = await ExecuteMethod(serviceProvider, serviceMethod, serviceInstance, arguments.Values.ToArray(), token);

            return result;
        }

        public MethodInfo GetServiceMethod(string service, string operation, Type attributeType)
        {
            return GetServiceMethod(GetServiceType(service), operation, attributeType);
        }

        #region Private Methods

        private MethodInfo GetServiceMethod(Type serviceType, string operation, Type attributeType)
        {
            if (string.IsNullOrWhiteSpace(operation))
                throw new ArgumentNullException("operation");

            // TODO: Performance (Cache?)
            var methods = serviceType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                .Where(x => operation.Equals(x.Name, StringComparison.InvariantCultureIgnoreCase) && x.GetCustomAttribute(attributeType, true) != null).ToList();

            if (methods.Count == 0)
            {
                methods = serviceType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                    .Where(x => operation.Equals(x.Name, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            if (methods.Count == 0)
                throw new Exception($"The service '{serviceType.FullName}' with the operation '{operation}' and the attribute '{attributeType.Name}' not found.");

            if (methods.Count > 1)
                throw new Exception($"The service '{serviceType.FullName}' with the operation '{operation}' and the attribute '{attributeType.Name}' found more than one time.");

            return methods.Single();
        }

        private async Task<object> ExecuteMethod(IServiceProvider serviceProvider, MethodInfo serviceMethod, object serviceInstance, object[] arguments, CancellationToken token = default)
        {
            if (serviceMethod == null)
                throw new ArgumentNullException(nameof(serviceMethod));

            var parms = serviceMethod.GetParameters();

            var args = new List<object>();
            var existingArgs = new List<object>(arguments);

            foreach (var p in parms)
            {
                var existingArg = existingArgs.FirstOrDefault(x => x.GetType() == p.ParameterType);

                if (existingArg != null)
                {
                    existingArgs.Remove(existingArg);
                    args.Add(existingArg);
                }
                else if (p.ParameterType == typeof(CancellationToken))
                {
                    args.Add(token);
                }
                else if (p.ParameterType.IsClass)
                {
                    var instance = serviceProvider.GetService(p.ParameterType) ?? Activator.CreateInstance(p.ParameterType, true);
                    args.Add(instance);
                }
                else
                {
                    args.Add(p.ParameterType.IsValueType ? Activator.CreateInstance(p.ParameterType) : null);
                }
            }

            if (serviceMethod.ReturnType == typeof(Task))
            {
                var task = (Task)serviceMethod.Invoke(serviceInstance, args.ToArray());
                await task.ConfigureAwait(false);

                return await Task.Run(() => task.GetAwaiter(), token);
            }
            else if (serviceMethod.ReturnType.IsGenericType && serviceMethod.ReturnType.BaseType == typeof(Task))
            {
                var task = (Task)serviceMethod.Invoke(serviceInstance, args.ToArray());
                await task.ConfigureAwait(false);
                return await Task.Run(() => (object)((dynamic)task).Result, token);
            }
            else
            {
                return await Task.Run(() => serviceMethod.Invoke(serviceInstance, args.ToArray()), token);
            }
        }

        private IDictionary<string, Type> GetServicesInner(Assembly[] assemblies)
        {
            var result = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase);

            var serviceBaseType = typeof(IService);
            foreach (var assembly in assemblies)
            {
                var services = assembly.GetTypes().Where(x => serviceBaseType.IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface).ToList();
                foreach (var service in services)
                {
                    // As a service name lets trancate 'Service' keyword
                    var serviceName = TrimServiceName(service.Name);
                    if (result.ContainsKey(serviceName))
                        throw new Exception($"Cannot register the service '{service.FullName}'. The service with the same name '{serviceName}' already exists. Service names must be unique.");

                    result.Add(serviceName, service);
                }
            }

            return result;
        }

        public static string TrimServiceName(string serviceName)
        {
            if (string.IsNullOrEmpty(serviceName))
                throw new ArgumentNullException(nameof(serviceName));

            return serviceName.TrimEnd("Service", StringComparison.InvariantCultureIgnoreCase);
        }

        #endregion Private Methods
    }
}
