﻿using Lotus.Abstract;
using System;

namespace Lotus.Core.Services
{
    public class DateTimeService : IService, IDateTime
    {
        public DateTimeService()
        {

        }

        public DateTime GetNowUtc()
        {
            return DateTime.UtcNow;
        }
    }
}
