﻿using Lotus.Abstract;
using Lotus.Sql.Services;
using Microsoft.Extensions.DependencyInjection;
using SqlKata;
using System;
using System.Threading.Tasks;

namespace Lotus.Core.Services
{
    public abstract class BaseService : IScopedService
    {
        private readonly IServiceProvider _services;

        public BaseService(IServiceProvider services)
        {
            _services = services;
        }

        public T GetService<T>()
        {
            return _services.GetRequiredService<T>();
        }

        public Query Query<T>() where T : class
        {
            return GetService<DbService>().Query<T>();
        }

        public async Task<string> SaveOrUpdateAsync<T>(T obj) where T : class
        {
            return await GetService<QueryService>().SaveOrUpdateAsync(obj);
        }

        public CoreConfigService Config => GetService<CoreConfigService>();
    }
}
