using Lotus.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Lotus.Core.Services
{
    /// <summary>
    /// AssemblyProvider provides with necessery assemblies and types.
    /// </summary>
    public class AssemblyProvider
    {
        public AssemblyProvider(CoreConfigService config)
        {
            Assemblies = GetRequiredAssemblies(config);
            Enums = GetEnums(Assemblies);
        }

        public Assembly[] Assemblies { get; }

        public IReadOnlyDictionary<string, Type> Enums { get; }

        #region Private Methods

        private Assembly[] GetRequiredAssemblies(CoreConfigService cfg)
        {
            var assemblies = new HashSet<Assembly>();

            assemblies.Add(typeof(LotusBootstrap).Assembly);

            foreach (var assemblyName in cfg.Settings.Modules)
            {
                var assembly = ReflectionHelper.GetAssemblyByName(assemblyName)
                    ?? throw new Exception($"Module '{assemblyName}' not found. See configuration file 'appsettings.json' => {nameof(CoreConfigService.Settings)} -> {nameof(CoreConfigService.Settings.Modules)}");

                assemblies.Add(assembly);
            }

            return assemblies.ToArray();
        }

        internal Type[] GetTypesByAttribute(Type attrType, bool inherit, bool noAbstract = true, bool noInterface = true)
        {
            return Assemblies
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => (!noAbstract || !x.IsAbstract) && (!noInterface || !x.IsInterface) && x.GetCustomAttribute(attrType, inherit) != null)
                .ToArray();
        }

        private IReadOnlyDictionary<string, Type> GetEnums(Assembly[] assemblies)
        {
            if (assemblies == null)
                throw new ArgumentNullException(nameof(assemblies));

            return Assemblies
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => !x.IsAbstract && !x.IsInterface && x.IsEnum)
                .ToDictionary(x => x.Name, x => x);
        }

        #endregion Private Methods
    }
}
