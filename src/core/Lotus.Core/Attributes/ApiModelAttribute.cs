using System;

namespace Lotus.Core.Attributes
{
    /// <summary>
    /// All classes or enums will be added into the swagger scheme api.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum)]
    public class ApiModelAttribute : Attribute
    {
    }
}
