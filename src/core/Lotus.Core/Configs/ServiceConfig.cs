﻿using Lotus.Abstract;
using Lotus.Core.Routing;
using Lotus.Core.Services;
using Lotus.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace Lotus.Core.Configs
{
    class ServiceConfig
    {
        internal static void ConfigureServices(IServiceCollection services, AssemblyProvider assemblyProvider)
        {
            RegisterAllServices(services, assemblyProvider.Assemblies);
        }

        /// <summary>
        /// Scans and registers all services derived from IService. No need to register all those services manually.
        /// </summary>
        /// <param name="assemblies">Assemblies which contain required services to register</param>
        private static IServiceCollection RegisterAllServices(IServiceCollection services, params Assembly[] assemblies)
        {
            // Types
            var baseServiceType = typeof(IService);
            var baseGenServiceType = typeof(IService<>);
            var scopedServiceType = typeof(IScopedService);
            var scopedGenServiceType = typeof(IScopedService<>);
            var transientServiceType = typeof(ITransientService);
            var transientGenServiceType = typeof(ITransientService<>);

            var serviceExecuter = new ServiceExecuter(assemblies);
            services.AddSingleton(serviceExecuter);

            foreach (var serviceClass in serviceExecuter.Services.Values)
            {
                // We register an interface if the service has one, otherwise it registers the service type itself.
                // Eg. "public class UserManager : IUserManager, IScopedService<IUserManager>" executes the following: "services.AddScoped(IUserManager, UserManager);"
                if (transientServiceType.IsAssignableFrom(serviceClass))
                {
                    var serviceInterface = transientGenServiceType.GetAssignableGenericArguments(serviceClass)?.FirstOrDefault();
                    if (serviceInterface != null)
                        services.AddTransient(serviceInterface, serviceClass);
                    services.AddTransient(serviceClass, serviceClass);
                }
                else if (scopedServiceType.IsAssignableFrom(serviceClass))
                {
                    var serviceInterface = scopedGenServiceType.GetAssignableGenericArguments(serviceClass)?.FirstOrDefault();
                    if (serviceInterface != null)
                        services.AddScoped(serviceInterface, serviceClass);
                    services.AddScoped(serviceClass, serviceClass);
                }
                else
                {
                    var serviceInterface = baseGenServiceType.GetAssignableGenericArguments(serviceClass)?.FirstOrDefault();
                    if (serviceInterface != null)
                        services.AddSingleton(serviceInterface, serviceClass);
                    services.AddSingleton(serviceClass, serviceClass);
                }
            }

            // Scan all routes and permissions
            var routeConfig = new RouteConfig(serviceExecuter.Services.Values);
            services.AddSingleton(routeConfig);

            //// Scan all SystemEvent methods
            //var systemEventConfig = new SystemEventConfiguration(serviceExecuter.Services.Values);
            //services.AddSingleton(systemEventConfig);

            //// Scan all Host Services (Service Workers)
            //var serviceWorkerType = typeof(BackgroundService);
            //var addHostedService = typeof(ServiceCollectionHostedServiceExtensions).GetMethod("AddHostedService", new Type[] { typeof(IServiceCollection) });
            //foreach (var assembly in assemblies)
            //{
            //    var workers = assembly.GetTypes().Where(x => !x.IsAbstract && !x.IsInterface && serviceWorkerType.IsAssignableFrom(x)).ToList();
            //    workers.ForEach(x => addHostedService.MakeGenericMethod(x).Invoke(null, new object[] { services }));
            //}

            return services;
        }
    }
}
