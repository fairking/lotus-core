﻿using Lotus.Core.Routing;
using Lotus.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace Lotus.Core.Configs
{
    class SwaggerConfig
    {
        internal static void ConfigureSwaggerServices(IServiceCollection services, CoreConfigService config)
        {
            var appServerUrl = config.Settings.AppServerUrl.Trim().TrimEnd('/');

            // Docs: https://github.com/domaindrivendev/Swashbuckle.AspNetCore Custom config: https://github.com/heldersepu/Swagger-Net-Test/blob/master/Swagger_Test/App_Start/SwaggerConfig.cs
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = $"{config.Settings.AppName ?? "Lotus"} API", Version = "v1" });
                c.TagActionsBy((api) =>
                {
                    var tags = new List<string>();
                    if (api.ActionDescriptor.RouteValues.ContainsKey("service"))
                        tags.Add(api.ActionDescriptor.RouteValues["service"]);
                    if (api.ActionDescriptor.RouteValues.ContainsKey("controller"))
                        tags.Add(api.ActionDescriptor.RouteValues["controller"]);
                    return tags;
                });
                //c.AddSecurityDefinition("Bearer",
                //    new ApiKeyScheme
                //    {
                //        In = "header",
                //        Description = "Bearer {Token JWT}",
                //        Name = "Authorization",
                //        Type = "apiKey",
                //    });
                c.DocumentFilter<SwaggerRouteDocumentFilter>();
                //c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                //{
                //    Name = "Bearer",
                //    Type = SecuritySchemeType.OAuth2,
                //    Flows = new OpenApiOAuthFlows() { Password = new OpenApiOAuthFlow() { TokenUrl = new Uri(appServerUrl + "/Common/User/Login") } },
                //    In = ParameterLocation.Header,
                //    Scheme = JwtBearerDefaults.AuthenticationScheme,
                //    // Optional scopes Scopes = new Dictionary<string, string> { { "api-name", "my api" }, }
                //});
                //c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                //{
                //    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                //    Name = "Authorization",
                //    In = ParameterLocation.Header,
                //    Type = SecuritySchemeType.ApiKey,
                //    BearerFormat = "Bearer {YOUR_TOKEN}"
                //});
                //c.AddSecurityRequirement(new OpenApiSecurityRequirement() { { new OpenApiSecurityScheme() { Name = "oauth2" }, new List<string>() }, });
                //c.AddSecurityRequirement(new OpenApiSecurityRequirement() { { new OpenApiSecurityScheme() { Name = "Bearer" }, new List<string>() }, });
                c.SchemaFilter<SwaggerValidationRules>();
            });
        }

        internal static void ConfigureSwaggerApp(IApplicationBuilder app, IWebHostEnvironment env, CoreConfigService config)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c =>
                {
                    c.RouteTemplate = "api/{documentName}/swagger.json";
                    //c.SerializeAsV2 = true;
                });
                app.UseSwaggerUI(c =>
                {
                    c.RoutePrefix = "api";
                    c.SwaggerEndpoint($"/api/v1/swagger.json", $"{config.Settings.AppName} API v1");
                    c.DocumentTitle = $"{config.Settings.AppName} Api";
                    c.EnableFilter();
                    c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                });
            }
        }
    }
}
