﻿using Lotus.Core.Services;
using Lotus.Helpers;
using Lotus.Msm.Events;
using Lotus.Msm.Jobs;
using Lotus.Msm.Services;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Lotus.Core.Configs
{
    public class MsmConfig
    {
        public static void ConfigureServices(IServiceCollection services, CoreConfigService appConfig, ServiceExecuter serviceExecuter, Action<IServiceCollection> jobServicesBuilder)
        {
            var jobServiceProvider = ConfigureJobsServices(jobServicesBuilder);

            services.AddHangfire((serviceProvider, configuration) =>
            {
                var jobActivator = new LotusJobActivator(jobServiceProvider);

                configuration
                    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                    .UseSimpleAssemblyNameTypeSerializer()
                    .UseRecommendedSerializerSettings()
                    .UseActivator(jobActivator)
                    .UseFilter(new JobClientSessionFilter())
                    .UseFilter(new JobServerSessionFilter(jobActivator))
                    .UseFilter(new AutomaticRetryAttribute() { DelaysInSeconds = new[] { 5, 15, 300, 1800 }, Attempts = 5 });

                var appCfg = serviceProvider.GetRequiredService<CoreConfigService>();

                configuration.UsePostgreSqlStorage(appCfg.Jobs.DbConnection);
            });

            if (appConfig.Jobs.MainWorkerCount > 0)
            {
                services.AddHangfireServer(options =>
                {
                    options.ServerName = $"{appConfig.Settings.AppInstanceName}_main";
                    options.WorkerCount = appConfig.Jobs.MainWorkerCount;
                    options.Queues = new[] { JobQueues.critical.ToString(), JobQueues.@default.ToString() };
                });
            }
            if (appConfig.Jobs.BackgroundWorkerCount > 0)
            {
                services.AddHangfireServer(options =>
                {
                    options.ServerName = $"{appConfig.Settings.AppInstanceName}_background";
                    options.WorkerCount = appConfig.Jobs.BackgroundWorkerCount;
                    options.Queues = new[] { JobQueues.background.ToString() };
                });
            }

            services.AddSingleton<IRecurringJobManager>(s => new RecurringJobManager());

            var systemEventConfig = new SystemEventConfiguration(serviceExecuter.Services.Values);
            services.AddSingleton(systemEventConfig);

            services.AddScoped<SystemEventService>();
            services.AddScoped<BackgroundJobService>();
        }

        public static void ConfigureApp(IApplicationBuilder app, IWebHostEnvironment env, CoreConfigService appConfig)
        {
            app.UseHangfireDashboard(
               pathMatch: "/jobs",
               options: new DashboardOptions()
               {
                   DashboardTitle = $"{appConfig.Settings.AppName ?? "Lotus"} Jobs",
                   AppPath = appConfig.Settings.AppClientUrl,
                   // The access to the dashboard is accessible only locally at the moment
                   // = new[] { new HangfireDashboardJwtAuthorizationFilter(CorePermissions.jobs_view) },
                });

            GlobalConfiguration.Configuration.UseNLogLogProvider();

            using (var scope = app.ApplicationServices.CreateScope())
            {
                ConfigureCron(scope.ServiceProvider);
                scope.ServiceProvider.GetRequiredService<SystemEventService>().TriggerEvent(new ApplicationStartupEvent()).RunSync();
                scope.ServiceProvider.GetRequiredService<BackgroundJobService>().CommitAllJobs();
            }
        }

        private static IServiceProvider ConfigureJobsServices(Action<IServiceCollection> jobServicesBuilder)
        {
            var services = new ServiceCollection();

            jobServicesBuilder?.Invoke(services);

            services.AddSingleton<IRecurringJobManager>(s => new RecurringJobManager());
            services.AddSingleton<IBackgroundJobClient>(s => new BackgroundJobClient());

            return services.BuildServiceProvider();
        }

        private static void ConfigureCron(IServiceProvider services)
        {
            // Recurring Jobs

            var config = services.GetRequiredService<CoreConfigService>();
            var eventConfig = services.GetRequiredService<SystemEventConfiguration>();
            var serviceExecuter = services.GetRequiredService<ServiceExecuter>();
            var backgroundJobService = services.GetRequiredService<BackgroundJobService>();

            var allCronJobs = eventConfig.GetCronJobs();
            foreach (var cron in allCronJobs)
            {
                var cronId = cron.Service + "." + cron.Operation;
                if (!config.Jobs.ExcludeRecurringTasks.Contains(cronId, StringComparison.OrdinalIgnoreCase))
                {
                    var methodInfo = serviceExecuter.GetServiceMethod(cron.Service, cron.Operation, typeof(CronAttribute));
                    backgroundJobService.AddUpdateRecurringJob(cronId, methodInfo, cron.CronAttribute.CronExpression);
                }
                else
                {
                    backgroundJobService.RemoveRecurringJob(cronId);
                }
            }
        }
    }
}
