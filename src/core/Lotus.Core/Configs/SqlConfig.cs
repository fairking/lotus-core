﻿using Lotus.Sql.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Data;

namespace Lotus.Core.Configs
{
    public class SqlConfig
    {
        public static void ConfigureServices(IServiceCollection services, DbService.DatabaseType dbType, string connectionString)
        {
            services.AddScoped<DbService>((srv) => {
                return new DbService(connectionString, dbType);
            });
            services.AddScoped<QueryService>();
            services.AddScoped<IDbConnection>(s => s.GetRequiredService<DbService>().Connection);
        }
    }
}
