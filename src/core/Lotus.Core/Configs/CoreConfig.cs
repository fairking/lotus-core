﻿using Lotus.Abstract;
using Lotus.Core.Auth;
using Lotus.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Lotus.Core.Configs
{
    public class CoreConfig
    {
        internal static void ConfigureServices(IServiceCollection services, CoreConfigService config, AssemblyProvider assemblyProvider, ServiceExecuter serviceExecuter)
        {
            services.AddSingleton<ILogger>(s => s.GetRequiredService<ILogger<LotusBootstrap>>());
            services.AddSingleton(config);
            services.AddSingleton(assemblyProvider);
            services.AddSingleton(serviceExecuter);
            services.AddSingleton(new DateTimeService());
            services.AddSingleton<IDateTime>(s => s.GetRequiredService<DateTimeService>());
            services.AddMemoryCache();

            services.AddScoped<UserContext>();
            services.AddScoped<IUserContext>(s => s.GetRequiredService<UserContext>());
            services.AddScoped<IUserContext>(s => s.GetRequiredService<UserContext>());
        }
    }
}
