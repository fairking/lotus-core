﻿using Lotus.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;
using System.Text.Json.Serialization;

namespace Lotus.Core.Configs
{
    public class AspNetConfig
    {
        internal static void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.AddHttpsRedirection(options =>
            {
                options.HttpsPort = 443;
            });

            services.AddHttpContextAccessor();
        }

        internal static void ConfigureApp(IApplicationBuilder app, IWebHostEnvironment env, CoreConfigService config)
        {
            var serveSpaApp = string.Equals(config.Settings.AppServerUrl, config.Settings.AppClientUrl, StringComparison.InvariantCultureIgnoreCase);

            // https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-apache?view=aspnetcore-3.1#configure-a-proxy-server
            if (config.Settings.Os == CoreConfigService.OsEnum.linux)
                app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto });

            if (!CoreConfigService.IsDevelopment)
                app.UseHsts();

            app.UseCors();

            app.UseHttpsRedirection();

            if (serveSpaApp)
            {
                app.UseDefaultFiles();
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "ClientApp")),
                    RequestPath = "",
                });
            }
            else
            {
                app.UseStaticFiles();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseStatusCodePages();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "Srv/{service}/{operation}",
                    defaults: new { controller = "Core", action = "Default" });
            });

            // handle client app side routes
            if (serveSpaApp)
            {
                app.Run(async (context) =>
                {
                    context.Response.ContentType = "text/html";
                    await context.Response.SendFileAsync(Path.Combine("ClientApp", "index.html"));
                });
            }
        }
    }
}
