﻿using Lotus.Sql.Services;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;

namespace Lotus.Core.Configs
{
    public class MigratorConfig
    {
        public static void ConfigureServices(IServiceCollection services, DbService.DatabaseType dbType, string connectionString, Assembly[] assemblies)
        {
            services
                // Add common FluentMigrator services
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => {
                    rb
                        // Set the connection string
                        .WithGlobalConnectionString(connectionString)
                        // Define the assembly containing the migrations
                        .ScanIn(assemblies).For.Migrations();

                    switch (dbType)
                    {
                        case DbService.DatabaseType.sqlite:
                            rb.AddSQLite();
                            break;

                        case DbService.DatabaseType.postgresql:
                            rb.AddPostgres();
                            break;

                        default:
                            throw new NotSupportedException($"The database type '{dbType}' is not implemented.");
                    }
                })
                // Enable logging to console in the FluentMigrator way
                //.AddLogging(lb => lb.AddFluentMigratorConsole()) // We use standart Logging from now (https://github.com/fluentmigrator/fluentmigrator/issues/930)
                ;
        }

        public static void ConfigureApp(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var services = scope.ServiceProvider;
                var logger = services.GetRequiredService<ILogger>();

                try
                {
                    var dbMigrations = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                    // Execute migrations
                    dbMigrations.MigrateUp();
                }
                catch (Exception ex)
                {
                    logger.LogCritical(ex, "Error whilst migrating database");
                    throw new Exception("Error whilst migrating database", ex);
                }
            }
        }
    }
}
