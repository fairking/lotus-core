using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;
using NLog.Web;
using LogLevel = NLog.LogLevel;
using Lotus.Core.Services;

namespace Lotus.Core.Configs
{
    class NLogConfig
    {
        internal static void ConfigureHost(IHostBuilder host)
        {
            host.ConfigureLogging((hosting, logging) =>
            {
                var appConfig = new CoreConfigService(hosting.Configuration);

                logging.ClearProviders();
                logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                NLogConfig.ConfigureInternal(appConfig);
            })
            .UseNLog();
        }

        private static void ConfigureInternal(CoreConfigService config)
        {
            var cfg = new NLog.Config.LoggingConfiguration();

            cfg.Variables.Add("instance_name", config.Settings.AppInstanceName);
            cfg.Variables.Add("version", CoreConfigService.AppVersion);
            cfg.Variables.Add("version_number", CoreConfigService.AppVersion.Replace("v.", "").Replace(".", "_"));
            cfg.Variables.Add("client_url", config.Settings.AppClientUrl);
            cfg.Variables.Add("server_url", config.Settings.AppServerUrl);

            // NullTarget (looks like it is not working)
            //var nullTarget = new NLog.Targets.NullTarget("null");
            //cfg.AddRule(LogLevel.Trace, LogLevel.Info, nullTarget, "Microsoft.*", true);
            //cfg.AddRule(LogLevel.Trace, LogLevel.Info, nullTarget, "System.*", true);
            //cfg.AddRule(LogLevel.Trace, LogLevel.Info, nullTarget, "Hangfire.*", true);
            //cfg.AddRule(LogLevel.Trace, LogLevel.Info, nullTarget, "NHibernate.*", true);

            // Console (https://github.com/NLog/NLog/wiki/Console-target)
            var consoleMinLevel = LogLevel.FromString(config.Logging.Console);
            if (consoleMinLevel != LogLevel.Off)
            {
                NLog.Common.InternalLogger.LogLevel = LogLevel.Error;
                NLog.Common.InternalLogger.LogToConsole = true;

                var consoleTarget = new NLog.Targets.ColoredConsoleTarget("console")
                {
                    Layout = "${date:format=yyyy-MM-dd HH\\:mm\\:ss}|${level:uppercase=true}|${logger:shortName=True}|${exception:format=Type}|${aspnet-user-identity}|${message}${newline}${exception:format=ToString}",
                };
                cfg.AddRule(consoleMinLevel, LogLevel.Fatal, consoleTarget);
            }

            // File (https://github.com/NLog/NLog/wiki/File-target)
            var fileMinLevel = LogLevel.FromString(config.Logging.File);
            if (fileMinLevel != LogLevel.Off)
            {
                NLog.Common.InternalLogger.LogFile = Path.Combine(config.Logging.FileDirectory, "nlog-internal-${shortdate}.log");
                var fileTarget = new NLog.Targets.FileTarget("file")
                {
                    FileName = Path.Combine(config.Logging.FileDirectory, "${var:instance_name}_${var:version_number}", "lotus-${shortdate}.log"),
                    KeepFileOpen = false,
                    MaxArchiveFiles = 30,
                    EnableFileDelete = true,
                    Encoding = Encoding.UTF8,
                    Layout = "${date:format=yyyy-MM-dd HH\\:mm\\:ss}|${level:uppercase=true}|${logger:shortName=True}|${exception:format=Type}|${aspnet-user-identity}|${message}${newline}${exception:format=ToString}",
                };
                cfg.AddRule(fileMinLevel, LogLevel.Fatal, fileTarget);
            }

            // Mail (https://github.com/NLog/NLog/wiki/Mail-target)
            var mailMinLevel = LogLevel.FromString(config.Logging.Mail);
            if (mailMinLevel != LogLevel.Off && !CoreConfigService.IsDevelopment)
            {
                if (mailMinLevel < LogLevel.Error) // Prevent from spam
                    throw new Exception("Cannot set min logging level for emails lower than Error. See appsettings.json => Logging => Mail.");

                if (string.IsNullOrWhiteSpace(config.Smtp.SaveEmailsToDiskPath) && !string.IsNullOrWhiteSpace(config.Smtp.Host) && !string.IsNullOrWhiteSpace(config.Logging.LogsEmail))
                {
                    var mailTarget = new NLog.MailKit.MailTarget("mail")
                    {
                        SmtpServer = config.Smtp.Host,
                        SmtpPort = config.Smtp.Port,
                        SmtpUserName = config.Smtp.Username,
                        SmtpPassword = config.Smtp.Password,
                        EnableSsl = config.Smtp.UseSsl,
                        SmtpAuthentication = NLog.MailKit.SmtpAuthenticationMode.Basic,
                        From = config.Smtp.EmailAddress,
                        To = config.Logging.LogsEmail,
                        Subject = "${level} from ${var:instance_name} ${var:version} ${message}",
                        Body = "${date:format=yyyy-MM-dd HH\\:mm\\:ss}|${level:uppercase=true}|${logger:shortName=True}|${exception:format=Type}|${message}${newline}${newline}"
                            + "LOG DETAILS:${newline}${exception:format=ToString}${newline}${newline}"
                            + "INFORMATION:${newline}User: ${aspnet-user-identity},${newline}IP: ${local-ip},${newline}Version: ${assembly-version:type=Informational},${newline}"
                                + "Environment: ${environment:variable=ASPNETCORE_ENVIRONMENT},${newline}Instance: ${var:instance_name},${newline}"
                                + "Server Url: ${var:server_url},${newline}Client Url: ${var:client_url},${newline}Db type: ${var:db_type},${newline}"
                                + "Db dialect: ${var:db_dialect},${newline}Db legacy: ${var:db_legacy},${newline}"
                                + "Url: ${aspnet-request-url:IncludePort=true:IncludeQueryString=true},${newline}${newline}${newline}",
                    };
                    cfg.AddRule(mailMinLevel, LogLevel.Fatal, mailTarget);
                }
            }

            NLog.LogManager.Configuration = cfg;
        }
    }
}
