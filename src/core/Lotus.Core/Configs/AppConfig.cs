﻿using Lotus.Core.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;

namespace Lotus.Core.Configs
{
    class AppConfig
    {
        internal static void ConfigureHost(IHostBuilder host, string[] args, Assembly mainAssembly)
        {
            host.ConfigureAppConfiguration((hosting, config) =>
            {
                CoreConfigService.AppVersion = mainAssembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion 
                    ?? mainAssembly.GetName().Version?.ToString(3) ?? "";

                CoreConfigService.IsDevelopment = hosting.HostingEnvironment.IsDevelopment();

                config.SetBasePath(hosting.HostingEnvironment.ContentRootPath);
                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                config.AddJsonFile($"appsettings.{hosting.HostingEnvironment.EnvironmentName ?? CoreConfigService.DevEnvName}.json", optional: true, reloadOnChange: true);
                
                config.AddEnvironmentVariables();

                config.AddCommandLine(args);

                if (string.Equals(hosting.HostingEnvironment.EnvironmentName, CoreConfigService.DevEnvName, StringComparison.OrdinalIgnoreCase))
                    config.AddUserSecrets("lotus");
            });
        }
    }
}
