using Lotus.Helpers;
using System;
using System.Linq.Expressions;

namespace Lotus.Core.Exceptions
{
    public class UserException : Exception
    {
        public string[] PropertyPaths { get; protected set; }
        public string[] Messages { get; protected set; }

        public UserException(string message) : base(message)
        {
            PropertyPaths = new[] { (string)null };
            Messages = new[] { message };
        }

        public UserException(string propertyPath, string message) : base(message + (!string.IsNullOrEmpty(propertyPath) ? " (" + propertyPath + ")" : ""))
        {
            PropertyPaths = new[] { propertyPath };
            Messages = new[] { message };
        }

        public UserException(string[] propertyPaths, string[] messages) : base(string.Join("; ", messages) + (propertyPaths != null && propertyPaths.Length > 0 ? " (" + string.Join(", ", propertyPaths) + ")" : ""))
        {
            PropertyPaths = propertyPaths;
            Messages = messages;
        }
    }

    public class UserException<TModel, TProperty> : UserException
    {
        public UserException(Expression<Func<TModel, TProperty>> propertyPath, string message) : base(message + (!string.IsNullOrEmpty(propertyPath.GetPropertyPath()) ? " (" + propertyPath.GetPropertyPath() + ")" : ""))
        {
            PropertyPaths = new[] { propertyPath.GetPropertyPath() };
            Messages = new[] { message };
        }
    }
}
