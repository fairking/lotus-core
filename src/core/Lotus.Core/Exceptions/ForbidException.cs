﻿using System;

namespace Lotus.Core.Exceptions
{
    public class ForbidException : Exception
    {
        public ForbidException() : base() { }

        public ForbidException(string message) : base(message) { }
    }
}
