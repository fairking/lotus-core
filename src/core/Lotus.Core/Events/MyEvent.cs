﻿using Lotus.Msm.Events;

namespace Lotus.Core.Events
{
    public class MyEvent : SystemEvent
    {
        public MyEvent() { }

        public string MyInfo { get; set; }
    }

    public class MyEventAttribute : SystemEventAttribute
    {
        public MyEventAttribute() : base(nameof(MyEvent), EventTypeEnum.AsyncBackground)
        {

        }
    }
}
