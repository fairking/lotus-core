﻿using Lotus.Core.ViewModels;
using Lotus.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Lotus.Core.Exceptions;
using Lotus.Core.Services;
using Lotus.Msm.Services;
using Lotus.Sql.Services;
using System.Reflection;

namespace Lotus.Core.Routing
{
    public class CoreController : Controller
    {
        protected bool _transactional;
        protected readonly DbService _dbService;
        protected readonly ILogger _logger;
        protected readonly IServiceProvider _services;
        protected readonly CoreConfigService _appConfig;
        protected readonly ServiceExecuter _serviceExecuter;
        protected readonly SystemEventService _eventService;
        protected readonly BackgroundJobService _jobQueueService;

        public CoreController(
            DbService dbService,
            ILogger logger,
            IServiceProvider services,
            CoreConfigService appConfig,
            ServiceExecuter serviceExecuter,
            SystemEventService eventService,
            BackgroundJobService jobQueueService
            )
        {
            _dbService = dbService;
            _logger = logger;
            _services = services;
            _appConfig = appConfig;
            _serviceExecuter = serviceExecuter;
            _eventService = eventService;
            _jobQueueService = jobQueueService;
        }

        [HttpGet]
        [HttpPost]
        [CoreAuthorization]
        [CoreException]
        public async Task<IActionResult> Default(string service, string operation)
        {
            var result = await _serviceExecuter.ExecuteService<RouteAttribute>(service, operation, _services, this);

            if (result is IActionResult)
                return result as IActionResult;

            return new ObjectResult(result);
        }

        [HttpGet]
        public IActionResult DefaultSpa()
        {
            var env = _services.GetService<IWebHostEnvironment>();
            return new PhysicalFileResult(Path.Combine(env.ContentRootPath, "ClientApp/index.html"), "text/html");
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var method = _serviceExecuter.GetServiceMethod(context.ActionArguments["service"]?.ToString(), context.ActionArguments["operation"]?.ToString(), typeof(RouteAttribute));

            var route = method.GetCustomAttribute<RouteAttribute>();

            if (route.Transactional)
            {
                _transactional = true;
                _dbService.BeginTransaction();
            }

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            if (_transactional)
            {
                if (context.Canceled || context.Exception != null)
                {
                    _logger.LogDebug("Rollback");
                    _dbService.Rollback();
                    _jobQueueService.RejectAllJobs();
                    _eventService.ClearQueue();
                }
                else
                {
                    var commited = false;
                    try
                    {
                        _logger.LogDebug("ExecuteAllEvents");
                        _eventService.ExecuteAllEvents().RunSync();

                        _logger.LogDebug("Commit");
                        _dbService.Commit();

                        commited = true;

                        _logger.LogDebug("CommitAllJobs");
                        _jobQueueService.CommitAllJobs(); // Do we need it in data readonly requests?
                    }
                    catch
                    {
                        _logger.LogDebug("Rollback");
                        if (!commited)
                            _dbService.Rollback();
                        _jobQueueService.RejectAllJobs();
                        _eventService.ClearQueue();

                        throw;
                    }
                }
            }
            else
            {
                // _transaction does not exists
            }
        }

        private string requestData;

        public override async Task<bool> TryUpdateModelAsync(object model, Type modelType, string prefix)
        {
            if (model == null)
                model = Activator.CreateInstance(modelType);

            var result = await base.TryUpdateModelAsync(model, modelType, prefix ?? string.Empty);

            if (model is FileUploadFormVm && Request.HasFormContentType && Request.Form.Files != null && Request.Form.Files.Count > 0)
            {
                (model as FileUploadFormVm).Files = Request.Form.Files;
            }

            if ("POST".Equals(Request.Method, StringComparison.OrdinalIgnoreCase) && !Request.HasFormContentType)
            {
                try
                {
                    if (requestData == null)
                    {
                        using (var reader = new StreamReader(Request.Body))
                            requestData = await reader.ReadToEndAsync();
                    }

                    if (!string.IsNullOrEmpty(requestData))
                    {
                        if (Request.ContentType != null && Request.ContentType.Contains("application/json"))
                            JsonConvert.PopulateObject(requestData, model, new JsonSerializerSettings() 
                            { 
                                //Culture = _currentUser.Culture 
                            });
                        else if (Request.ContentType != null && Request.ContentType.Contains("application/xml"))
                            model = requestData.XmlDeserialize(modelType, model);
                    }

                    result = true;
                }
                catch (JsonSerializationException js_exc)
                {
                    _logger.LogWarning(js_exc, "Error occurred while parsing '{0}' model.", modelType.Name);
                    result = false;
                    throw new UserException(js_exc.Message);
                }
                catch (JsonReaderException jr_exc)
                {
                    _logger.LogWarning(jr_exc, "Error occurred while parsing '{0}' model.", modelType.Name);
                    result = false;
                    throw new UserException(jr_exc.Message);
                }
                catch (Exception exc)
                {
                    _logger.LogError(exc, "Error occurred while parsing '{0}' model.", modelType.Name);
                    result = false;
                }
            }

            model.TrimObject();

            return result;
        }

        [NonAction]
        public virtual Task<bool> TryUpdateModelAsync(object model, Type modelType)
        {
            return TryUpdateModelAsync(model, modelType, null);
        }

        public override Task<bool> TryUpdateModelAsync<TModel>(TModel model, string prefix)
        {
            return TryUpdateModelAsync(model, typeof(TModel), prefix);
        }

        public override Task<bool> TryUpdateModelAsync<TModel>(TModel model)
        {
            return TryUpdateModelAsync(model, typeof(TModel));
        }
    }
}
