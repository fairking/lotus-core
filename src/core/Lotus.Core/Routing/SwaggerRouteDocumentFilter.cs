using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Lotus.Core.Attributes;
using Lotus.Core.Services;

namespace Lotus.Core.Routing
{
    // See Swagger API for any info https://swagger.io/specification/#swaggerObject
    public class SwaggerRouteDocumentFilter : IDocumentFilter
    {
        private readonly AssemblyProvider _assemblyProvider;
        private readonly RouteConfig _routeConfiguration;
        private readonly CoreConfigService _appConfig;
        private static List<Type> apiTypes;
        private const string ServiceSuffix = "Service";

        public SwaggerRouteDocumentFilter(
            AssemblyProvider assemblyProvider,
            RouteConfig routeConfiguration,
            CoreConfigService appConfig)
        {
            _assemblyProvider = assemblyProvider;
            _routeConfiguration = routeConfiguration;
            _appConfig = appConfig;
        }

        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext filterContext)
        {
            // Remove all autogenerated Paths
            swaggerDoc.Paths.Clear();

            // Add free floating Models
            RegisterAllApiModels(filterContext, _assemblyProvider.Assemblies);

            // Add data routes
            foreach (var routeOption in _routeConfiguration.GetRouteOptions().Values)
            {
                if (routeOption.AllowAnonymous || (CoreConfigService.IsDevelopment || ClaimsPrincipal.Current != null || _appConfig.Security.EnableApiScheme))
                    swaggerDoc.Paths.Add("Srv/" + routeOption.Route, CreatePathItem(routeOption, filterContext));
            }
        }

        private OpenApiPathItem CreatePathItem(RouteOption routeOption, DocumentFilterContext filterContext)
        {
            var responseContent = new Dictionary<string, OpenApiMediaType>();
            if (typeof(FileResult).IsAssignableFrom(routeOption.Response))
            {
                responseContent.Add("application/octet-stream", new OpenApiMediaType() { Schema = new OpenApiSchema() { Type = "string", Format = "binary" } });
            }
            else
            {
                responseContent.Add("application/json", new OpenApiMediaType() { Schema = GetOrAddTypeSchema(filterContext, routeOption.Response) });
                responseContent.Add("application/xml", new OpenApiMediaType() { Schema = GetOrAddTypeSchema(filterContext, routeOption.Response) });
            }

            var x = new OpenApiPathItem();
            x.AddOperation(OperationType.Post, new OpenApiOperation()
            {
                Tags = new[] { new OpenApiTag() { Name = routeOption.Service + ServiceSuffix } },
                OperationId = $"{routeOption.Operation}",
                RequestBody = routeOption.Parameters.Any() ? new OpenApiRequestBody()
                {
                    Required = true,
                    Content = {
                        { "application/json", new OpenApiMediaType() { Schema = GetOrAddTypeSchema(filterContext, routeOption.Parameters.Values.First()) } },
                        { "application/xml", new OpenApiMediaType() { Schema = GetOrAddTypeSchema(filterContext, routeOption.Parameters.Values.First()) } },
                        { "application/x-www-form-urlencoded", new OpenApiMediaType() { Schema = GetOrAddTypeSchema(filterContext, routeOption.Parameters.Values.First()) } },
                    },
                } : null,
                Security = !routeOption.AllowAnonymous ? GetSecurityRequirements() : null,
                Responses = new OpenApiResponses()
                {
                    {
                        "200", new OpenApiResponse()
                        {
                            Description = "OK",
                            Content = responseContent,
                        }
                    },
                },
                Description = routeOption.Description,
            });
            return x;
        }

        private IList<OpenApiSecurityRequirement> GetSecurityRequirements()
        {
            return new List<OpenApiSecurityRequirement>()
            {
                new OpenApiSecurityRequirement() { { new OpenApiSecurityScheme() { Name = "Bearer" }, new List<string>() }, },
                // APi keys not implemented

                //new OpenApiSecurityRequirement() { { new OpenApiSecurityScheme()
                //{
                //    Name = ApiKeyAuthSchemeOptions.AuthenticationScheme,
                //    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = ApiKeyAuthSchemeOptions.AuthenticationScheme },
                //}, new List<string>() }, },
            };
        }

        private void RegisterAllApiModels(DocumentFilterContext filterContext, Assembly[] assemblies)
        {
            if (apiTypes == null)
            {
                apiTypes = new List<Type>();
                foreach (var assembly in assemblies)
                {
                    var allTypes = assembly.GetTypes();
                    var apiModels = allTypes
                        .Where(x => x.IsPublic && !x.IsAbstract && !x.IsInterface && x.GetCustomAttribute<ApiModelAttribute>() != null)
                        .ToArray();
                    foreach (var apiModelType in apiModels)
                        apiTypes.Add(apiModelType);
                }
            }

            foreach (var apiModelType in apiTypes)
                GetOrAddTypeSchema(filterContext, apiModelType);
        }

        private OpenApiSchema GetOrAddTypeSchema(DocumentFilterContext filterContext, Type type)
        {
            var result = type != null ? filterContext.SchemaGenerator.GenerateSchema(type, filterContext.SchemaRepository) : null;

            return result;
        }

    }
}
