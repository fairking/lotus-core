using System;

namespace Lotus.Core.Routing
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.ReturnValue | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class RouteAttribute : Attribute
    {
        public RouteAttribute() : this(new object[0])
        {
        }

        public RouteAttribute(params object[] perms)
        {
            Perms = perms;
        }

        /// <summary>
        /// List of permissions required to authorize the request (eg. "User", "Admin"). "*" means any.
        /// </summary>
        public object[] Perms { get; set; }

        /// <summary>
        /// You can apply authorization policy to the request (comma separated, eg. "IAm18", "IHavePaymentMethod"). "*" means any.
        /// </summary>
        public string Policies { get; set; }

        /// <summary>
        /// The authentication is not required during the request
        /// </summary>
        public bool AllowAnonymous { get; set; }

        /// <summary>
        /// Use transactional route if you need to open a database transaction during the request
        /// </summary>
        public bool Transactional { get; set; }
    }
}
