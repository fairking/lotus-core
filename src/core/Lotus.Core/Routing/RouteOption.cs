using System;
using System.Collections.Generic;

namespace Lotus.Core.Routing
{
    public class RouteOption
    {
        public RouteOption()
        {
            Perms = new HashSet<string>();
            Policies = new HashSet<string>();
        }

        public string Route { get; set; }
        public string Service { get; set; }
        public string Operation { get; set; }
        public HashSet<string> Perms { get; set; }
        public HashSet<string> Policies { get; set; }
        public bool AllowAnonymous { get; set; }
        public string Description { get; set; }

        public Type Response { get; set; }
        public IDictionary<string, Type> Parameters { get; set; }

    }
}
