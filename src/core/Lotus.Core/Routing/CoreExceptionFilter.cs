using Lotus.Core.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Lotus.Core.Routing
{
    public class CoreExceptionFilter : IAsyncExceptionFilter
    {
        private readonly ILogger<CoreExceptionFilter> _logger;

        public CoreExceptionFilter(ILogger<CoreExceptionFilter> logger)
        {
            _logger = logger;
        }

        public async Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.Exception != null && !context.ExceptionHandled)
            {
                if (context.Exception is UserException)
                {
                    _logger.LogWarning(context.Exception, context.Exception.Message);
                    context.Result = new UserErrorResult(context.Exception as UserException);
                }
                // Generalnie not found ma być błędem systemowym (patrz ostatni else). Np kiedy entity albo serwis nie został znaleziony to ma byc błąd systemowy a nie not found.
                // Klient powinien być tylko powiadamiany o PageNotFound co już zostało zrobione po stronie klienta.
                //else if (context.Exception is NotFoundException)
                //{
                //    _logger.LogWarning(context.Exception, context.Exception.Message);
                //    context.Result = new NotFoundObjectResult(context.Exception as NotFoundException);
                //}
                else if (context.Exception is ForbidException)
                {
                    _logger.LogWarning(context.Exception, context.Exception.Message);
                    context.Result = new ForbidResult();
                }
                else if (context.Exception is UnauthorizedException)
                {
                    _logger.LogWarning(context.Exception, context.Exception.Message);
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    _logger.LogError(context.Exception, context.Exception.Message);
                    context.Result = new SystemErrorResult();
                }
                context.ExceptionHandled = true;
            }
        }
    }

    public class CoreExceptionAttribute : TypeFilterAttribute
    {
        public CoreExceptionAttribute() : base(typeof(CoreExceptionFilter))
        {

        }
    }
}
