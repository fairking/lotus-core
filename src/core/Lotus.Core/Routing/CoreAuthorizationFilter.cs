using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Lotus.Core.Routing
{
    public class CoreAuthorizationFilter : IAsyncAuthorizationFilter
    {
        private readonly ILogger<CoreAuthorizationFilter> _logger;

        public CoreAuthorizationFilter(ILogger<CoreAuthorizationFilter> logger)
        {
            _logger = logger;
        }

        public virtual async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var controller = context.RouteData.Values["controller"]?.ToString();
            var service = context.RouteData.Values["service"]?.ToString();
            var operation = context.RouteData.Values["operation"]?.ToString();

            //if (!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(service) && !string.IsNullOrEmpty(operation))
            //{
            //    await _securityService.AuthenticateAsync(context.HttpContext);

            //    var result = await _tranSecurityService.Validate($"{controller}/{service}/{operation}");
            //    if (result.code != 200)
            //        context.Result = new StatusCodeResult(result.code);
            //}
            //else
            //{
            //    context.Result = new BadRequestResult();
            //}
        }
    }

    public class CoreAuthorizationAttribute : TypeFilterAttribute
    {
        public CoreAuthorizationAttribute() : base(typeof(CoreAuthorizationFilter))
        {
        }
    }
}
