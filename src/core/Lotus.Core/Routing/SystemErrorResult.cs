﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lotus.Core.Routing
{
    public class SystemErrorResult : StatusCodeResult
    {
        public SystemErrorResult() : base(StatusCodes.Status500InternalServerError) { }
    }
}
