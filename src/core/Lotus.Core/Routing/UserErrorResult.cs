﻿using Lotus.Core.Exceptions;
using Lotus.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Lotus.Core.Routing
{
    public class UserErrorResult : BadRequestObjectResult
    {
        public UserErrorResult(string[] messages) : base(messages.Select(x => new ErrorVm() { Message = x }).ToArray()) { }

        public UserErrorResult(string[] propertyPaths, string[] messages) : base(messages.Select((x, idx) => new ErrorVm() { PropertyPath = propertyPaths[idx], Message = x }).ToArray()) { }

        public UserErrorResult(string message) : this(new[] { message }) { }

        public UserErrorResult(string propertyPath, string message) : this(new[] { propertyPath }, new[] { message }) { }

        public UserErrorResult(UserException userException) : this(userException.PropertyPaths, userException.Messages) { }
    }
}
