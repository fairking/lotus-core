using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Lotus.Core.Attributes;
using Lotus.Helpers;

namespace Lotus.Core.Routing
{
    public class RouteConfig
    {
        private readonly Type _genericTaskType = typeof(Task<>);
        private readonly Type _taskType = typeof(Task);
        private readonly Type _voidType = null;

        private IDictionary<string, RouteOption> _routeOptions;

        public RouteConfig()
        {
            _routeOptions = new Dictionary<string, RouteOption>(StringComparer.OrdinalIgnoreCase);
        }

        public RouteConfig(IEnumerable<Type> serviceClasses) : this()
        {
            Configure(serviceClasses);
        }

        public void Configure(IEnumerable<Type> serviceClasses)
        {
            foreach (var serviceClass in serviceClasses)
                Configure(serviceClass);
        }

        public void Configure(Type serviceClass)
        {
            var serviceName = serviceClass.Name.TrimEnd("Service", StringComparison.InvariantCultureIgnoreCase);
            var routeMethods = serviceClass.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach (var routeMethod in routeMethods)
            {
                var routeAttribute = routeMethod.GetCustomAttribute<RouteAttribute>();
                if (routeAttribute != null)
                {
                    var key = $"{serviceName}/{routeMethod.Name}";

                    var responseType = routeMethod.ReturnType;
                    if (responseType.IsGenericType && responseType.GetGenericTypeDefinition() == _genericTaskType)
                        responseType = responseType.GenericTypeArguments.FirstOrDefault();
                    if (responseType == _taskType)
                        responseType = _voidType;
                    if (responseType == typeof(void))
                        responseType = _voidType;

                    var parameters = routeMethod.GetParameters().ToDictionary(x => x.Name, x => x.ParameterType);

                    _routeOptions.Add(
                        key,
                        new RouteOption()
                        {
                            Route = key,
                            Service = serviceName,
                            Operation = routeMethod.Name,
                            Perms = (routeAttribute.Perms?.Length ?? 0) > 0 ? new HashSet<string>(routeAttribute.Perms.Select(x => x.ToString())) : new HashSet<string>(),
                            Policies = !string.IsNullOrWhiteSpace(routeAttribute.Policies) ? new HashSet<string>(routeAttribute.Policies.Split(',', StringSplitOptions.RemoveEmptyEntries)) : new HashSet<string>(),
                            AllowAnonymous = routeAttribute.AllowAnonymous,
                            Response = responseType,
                            Parameters = parameters,
                            Description = routeMethod.GetCustomAttribute<DescriptionAttribute>()?.Description,
                        });
                }
            }
        }

        public IReadOnlyDictionary<string, RouteOption> GetRouteOptions()
        {
            return (IReadOnlyDictionary<string, RouteOption>)_routeOptions;
        }

        public RouteOption GetRouteOption(string routeAction)
        {
            RouteOption result;
            _routeOptions.TryGetValue(routeAction, out result);
            return result;
        }
    }
}
