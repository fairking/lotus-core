﻿using Lotus.Abstract;
using SqlKata;
using System;

namespace Lotus.Core.Entities
{
    public class BaseEntity : IEntity, IAudit, IDeletable
    {
        [Key]
        public virtual string Id { get; protected set; }

        public virtual string CreatedBy { get; protected set; }

        public virtual string UpdatedBy { get; protected set; }

        public virtual DateTime CreatedTime { get; protected set; }

        public virtual DateTime UpdatedTime { get; protected set; }

        public virtual int Version { get; protected set; }

        public virtual bool IsDeleted { get; protected set; }
    }
}
