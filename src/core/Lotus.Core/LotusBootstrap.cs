﻿using Lotus.Core.Configs;
using Lotus.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;

namespace Lotus.Core
{
    public class LotusBootstrap : IDisposable
    {
        private CoreConfigService _config;
        private AssemblyProvider _assemblyProvider;
        private ServiceExecuter _serviceExecuter;

        public LotusBootstrap(IConfiguration config)
        {
            _config = new CoreConfigService(config);
            _assemblyProvider = new AssemblyProvider(_config);
            _serviceExecuter = new ServiceExecuter(_assemblyProvider.Assemblies);
        }

        public static void ConfigureHost(IHostBuilder host, string[] args, Assembly mainAssembly)
        {
            AppConfig.ConfigureHost(host, args, mainAssembly);
            NLogConfig.ConfigureHost(host);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            CoreConfig.ConfigureServices(services, _config, _assemblyProvider, _serviceExecuter);
            AspNetConfig.ConfigureServices(services);
            SqlConfig.ConfigureServices(services, _config.Database.Type, _config.Database.Connection);
            MigratorConfig.ConfigureServices(services, _config.Database.Type, _config.Database.Connection, _assemblyProvider.Assemblies);
            SwaggerConfig.ConfigureSwaggerServices(services, _config);
            ServiceConfig.ConfigureServices(services, _assemblyProvider);
            MsmConfig.ConfigureServices(services, _config, _serviceExecuter, (sc) =>
            {
                CoreConfig.ConfigureServices(sc, _config, _assemblyProvider, _serviceExecuter);
                SqlConfig.ConfigureServices(sc, _config.Database.Type, _config.Database.Connection);
                ServiceConfig.ConfigureServices(sc, _assemblyProvider);
            });
        }

        public void ConfigureApp(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var logger = app.ApplicationServices.GetRequiredService<ILogger>();
            logger.LogInformation($"APPSTART ENVIRONMENT: {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? CoreConfigService.DevEnvName}, VERSION: {CoreConfigService.AppVersion}");

            MigratorConfig.ConfigureApp(app.ApplicationServices);
            SwaggerConfig.ConfigureSwaggerApp(app, env, _config);
            MsmConfig.ConfigureApp(app, env, _config);
            AspNetConfig.ConfigureApp(app, env, _config);
        }

        public void Dispose()
        {
            // Nothing to do
        }
    }
}
