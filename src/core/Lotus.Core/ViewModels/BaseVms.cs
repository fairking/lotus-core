using Lotus.Core.Attributes;
using Lotus.Core.Validation;
using System;

namespace Lotus.Core.ViewModels
{
    public interface IFormVm { }

    public interface IRowVm { }

    public interface IQueryVm { }

    public interface IPageVm { }

    public abstract class BaseRowVm : IRowVm
    {
        public virtual string Id { get; set; }
    }

    public abstract class BaseEntityRowVm : BaseRowVm
    {
        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public virtual DateTime UpdatedDate { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        public virtual int Version { get; set; }
    }

    public abstract class BaseCreateFormVm : IFormVm
    {
    }

    public abstract class BaseEditFormVm : IFormVm
    {
        [ValRequired]
        [ValLength(50)]
        public virtual string Id { get; set; }
    }

    public abstract class BasePageVm : IPageVm
    {

    }

    public class QueryVm : IQueryVm
    {
        public QueryVm()
        {
        }

        public QueryVm(int page, int pageSize, string[] sorts)
        {
            Page = page;
            PageSize = pageSize;
            Sorts = sorts;
        }

        /// <summary>
        /// Page number starts from 1. 0 will give zero results with row count only.
        /// </summary>
        [ValRange(0, int.MaxValue)]
        public int Page { get; set; }

        [ValRange(0, 1000)]
        public int PageSize { get; set; }

        public string[] Sorts { get; set; }

        [ValLength(250)]
        public string Search { get; set; }
    }

    public class IdVm : IQueryVm
    {
        [ValRequired]
        public string Id { get; set; }
    }

    public class IdsVm : IQueryVm
    {
        [ValRequired]
        public string[] Ids { get; set; }
    }

    [ApiModel]
    public class ErrorVm : IPageVm
    {
        public string PropertyPath { get; set; }
        public string Message { get; set; }
    }
}
