﻿using Lotus.Core.Validation;
using System;
using System.Collections.Generic;

namespace Lotus.Core.ViewModels
{
    [Serializable]
    public class LookupItemVm : BaseRowVm
    {
        public LookupItemVm()
        {
        }

        public LookupItemVm(string id)
        {
            Id = id;
        }

        public LookupItemVm(string id, string name) : this(id)
        {
            Name = name;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool Disabled { get; set; }
        public string Icon { get; set; }
    }

    public class LookupVm : BasePageVm
    {
        public IEnumerable<LookupItemVm> Items { get; }

        public LookupVm()
        {
            Items = new List<LookupItemVm>();
        }

        public LookupVm(IEnumerable<LookupItemVm> items)
        {
            Items = items;
        }
    }

    public class LookupQueryVm : IQueryVm
    {
        public LookupQueryVm()
        {
        }

        public string Search { get; set; }
    }

    public class EnumLookupVm : IFormVm
    {
        [ValRequired]
        public string EnumType { get; set; }
    }
}
