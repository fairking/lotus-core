﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Lotus.Core.ViewModels
{
    public class FileUploadFormVm
    {
        public FileUploadFormVm()
        {
        }

        public IEnumerable<IFormFile> Files { get; set; }
    }

    public class FileUploadVm
    {
        public string AttachmentId { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
    }
}
