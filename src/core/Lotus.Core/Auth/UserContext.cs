﻿using Lotus.Abstract;
using System;
using System.Globalization;

namespace Lotus.Core.Auth
{
    public class UserContext : IUserContext
    {
        public string UserId { get; protected set; }
        public string TimeZone { get; protected set; }
        public string Culture { get; protected set; }

        public UserContext()
        {
            TimeZone = TimeZoneInfo.Local.Id;
            Culture = CultureInfo.CurrentCulture.IetfLanguageTag;
        }

        public void SetUser(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            UserId = user.Id;
            TimeZone = user.TimeZone;
            Culture = user.Culture;
        }

        public void SetUser(string userId, string timezone, string culture)
        {
            UserId = userId;
            TimeZone = timezone;
            Culture = culture;
        }
    }
}
