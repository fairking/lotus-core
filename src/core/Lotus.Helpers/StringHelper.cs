using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Lotus.Helpers
{
    public static class StringHelper
    {
        public static string ToFormat(this string str, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;

            return string.Format(str, args);
        }

        public static string TryToFormat(this string str, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;

            var result = str;
            if (args != null && args.Length > 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    result = result.Replace("{" + i + "}", args[i].ToString());
                }
            }
            return result;
        }

        public static string TryFormat(this string srt, params object[] args)
        {
            try
            {
                return string.Format(srt, args);
            }
            catch { }

            return srt;
        }

        public static string TryFormat(this string srt, IFormatProvider provider, params object[] args)
        {
            try
            {
                return string.Format(provider, srt, args);
            }
            catch { }

            return srt;
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static string TrimNullable(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return str.Trim();
        }

        public static string ToNullableString(this object str, string defaultStr = null)
        {
            if (str == null)
                return defaultStr;

            return str.ToString();
        }

        public static T TryParse<T>(this string str, T defaultValue = default(T), IFormatProvider provider = null)
        {
            object result = null;
            try
            {
                if (provider != null)
                    result = Convert.ChangeType(str, typeof(T), provider);
                else
                    result = Convert.ChangeType(str, typeof(T));
            }
            catch { }

            return (T)result;
        }

        public static string ToMD5Hash(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            return Encoding.ASCII.GetBytes(str).ToMD5Hash();
        }

        public static string ToMD5Hash(this byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return null;

            using (var md5 = MD5.Create())
            {
                return string.Join("", md5.ComputeHash(bytes).Select(x => x.ToString("X2")));
            }
        }

        public static string ToSha512Hash(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                throw new ArgumentNullException(nameof(str));

            using (var alg = SHA512.Create())
            {
                var strBytes = Encoding.UTF8.GetBytes(str);
                alg.ComputeHash(strBytes);
                return BitConverter.ToString(alg.Hash);
            }
        }

        public static IEnumerable<string> FromDelimitedList(this string str, string[] separators, StringSplitOptions options = StringSplitOptions.RemoveEmptyEntries)
        {
            if (string.IsNullOrWhiteSpace(str))
                return new List<string>();

            return str.Split(separators, options);
        }

        public static string ToDelimitedList<T>(this IEnumerable<T> list, string separator = ",", string quote = "")
        {
            if (!list.Any())
                return null;

            string result = "";

            foreach (var el in list)
            {
                if (el == null)
                    continue;

                if (!string.IsNullOrEmpty(result))
                    result += ",";

                result += (quote + el.ToString() + quote);
            }
            return result;
        }

        public static MailAddress ToMailAddress(this string emailAddress, string displayName = null)
        {
            return string.IsNullOrEmpty(displayName)
                ? new MailAddress(emailAddress?.Replace("(", "<").Replace(")", ">"))
                : new MailAddress(emailAddress?.Replace("(", "<").Replace(")", ">"), displayName: displayName);
        }

        public static bool IsEmailAddress(this string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
                return false;

            try
            {
                emailAddress?.ToMailAddress();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsPhoneNumber(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return false;

            var r = new Regex(@"(?<!\w)(\(?(\+|00)?48\)?)?[ -]?\d{3}[ -]?\d{3}[ -]?\d{3}(?!\w)");
            return r.IsMatch(str);
        }

        public static (string, string) ToFirstLastName(this string fullName)
        {
            var nameParts = fullName.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return (nameParts.Length > 0 ? nameParts[0] : "", nameParts.Length > 1 ? string.Join(" ", nameParts.Except(new[] { nameParts[0] })) : "");
        }

        public static T XmlDeserialize<T>(this string toDeserialize, T mergeTo = null) where T : class
        {
            return (T)toDeserialize.XmlDeserialize(typeof(T), mergeTo);
        }

        public static string XmlSerialize<T>(this T toSerialize)
        {
            return toSerialize.XmlSerialize();
        }

        public static object XmlDeserialize(this string toDeserialize, Type objectType, object mergeTo = null)
        {
            object result;

            XmlSerializer xmlSerializer = new XmlSerializer(objectType);
            using (StringReader textReader = new StringReader(toDeserialize))
            {
                result = xmlSerializer.Deserialize(textReader);
            }

            if (mergeTo != null)
                result = mergeTo.MergeObjects(result);

            return result;
        }

        public static string XmlSerialize(this object toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static object JsonDeserialize(this string toDeserialize, Type type)
        {
            return JsonConvert.DeserializeObject(toDeserialize, type);
        }

        public static string JsonSerialize(this object toSerialize)
        {
            return JsonConvert.SerializeObject(toSerialize);
        }

        public static T JsonDeserialize<T>(this string toDeserialize)
        {
            return JsonConvert.DeserializeObject<T>(toDeserialize);
        }

        public static string JsonSerialize<T>(this T toSerialize)
        {
            return JsonConvert.SerializeObject(toSerialize);
        }

        public static string CombineUrl(this string baseUrl, string relativeUrl)
        {
            // https://stackoverflow.com/questions/372865/path-combine-for-urls
            return new Uri(new Uri(baseUrl), relativeUrl.TrimEnd('/')).ToString();
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_ \\-.]+", "", RegexOptions.Compiled);
        }

        /// <summary>
        /// Removes the start part of the string, if it is matchs, otherwise leave string unchanged
        /// </summary>
        public static string TrimStart(this string str, string startValue, StringComparison stringComparison = default)
        {
            if (str.StartsWith(startValue, stringComparison))
            {
                str = str.Remove(0, startValue.Length);
            }
            return str;
        }

        /// <summary>
        /// Removes the end part of the string, if it is matchs, otherwise leave string unchanged
        /// </summary>
        public static string TrimEnd(this string str, string endValue, StringComparison stringComparison = default)
        {
            if (str.EndsWith(endValue, stringComparison))
            {
                str = str.Remove(str.Length - endValue.Length, endValue.Length);
            }
            return str;
        }

        public static string RemoveNewLines(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return str.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Replace("\t", " ").Trim();
        }

        public static string Trancate(this string str, int length)
        {
            if (length <= 5)
                throw new ArgumentException("length must be at least 5");

            if (string.IsNullOrEmpty(str))
                return str;

            if (str.Length < length)
                return str;

            return str.Substring(0, length - 3) + "...";
        }

        public static string BrToNewLine(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return str.Replace("<br />", Environment.NewLine);
        }

        public static string NewLineToBr(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return str.Replace(@"\r\n", "<br />").Replace(@"\r", "<br />").Replace(@"\n", "<br />");
        }

        /// <summary>
        /// Convert any object to string.
        /// </summary>
        public static string ConvertToString(this object value)
        {
            if (value == null)
                return null;

            var type = Nullable.GetUnderlyingType(value.GetType()) ?? value.GetType();

            if (type == typeof(DateTime))
                return ((DateTime)value).ToString("o");
            else if (typeof(IConvertible).IsAssignableFrom(type))
                return ((IConvertible)value).ToString(CultureInfo.InvariantCulture);
            else
                return value.JsonSerialize();
        }

        public static T ConvertFromString<T>(this string value)
        {
            return (T)ConvertFromString(value, typeof(T));
        }

        public static object ConvertFromString(this string value, Type t)
        {
            if (value == null)
                return ReflectionHelper.GetDefaultValue(t);

            var type = Nullable.GetUnderlyingType(t) ?? t;

            if (type == typeof(DateTime))
            {
                DateTime dt;
                if (DateTime.TryParseExact(value, "o", CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind, out dt))
                    return dt;
                else
                    return null;
            }
            else if (typeof(IConvertible).IsAssignableFrom(type))
            {
                return Convert.ChangeType(value, type, CultureInfo.InvariantCulture);
            }
            else
            {
                return value.JsonDeserialize(type);
            }
        }

        public static string HtmlToText(this string html)
        {
            // http://www.beansoftware.com/ASP.NET-Tutorials/Convert-HTML-To-Plain-Text.aspx

            if (string.IsNullOrEmpty(html))
                return html;

            // Remove new lines since they are not visible in HTML
            html = html.Replace("\r\n", " ");
            html = html.Replace("\r", " ");
            html = html.Replace("\n", " ");

            // Remove tab spaces
            html = html.Replace("\t", " ");

            // Remove multiple white spaces from HTML
            html = Regex.Replace(html, "\\s+", " ");

            // Remove HEAD tag
            html = Regex.Replace(html, "<head.*?</head>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Remove any scripts
            html = Regex.Replace(html, "<script.*?</script>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Remove any styles
            html = Regex.Replace(html, "<style.*?</style>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Replace
            // <br/>
            // tag with new line
            html = Regex.Replace(html, @"(<br>|<br />|<br/>|</ br>|</br>)", "\r\n", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Add a new line before every <p> tag
            html = Regex.Replace(html, @"(<p>|<p )", "\r\n$1", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Add a space after every </div> tag
            html = Regex.Replace(html, @"(</div>|</ div>)", "$1 ", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            // Replace special characters like &, <, >, " etc.
            StringBuilder sbHTML = new StringBuilder(html);
            // Note: There are many more special characters, these are just most common. You can add new characters in
            // this arrays if needed
            string[] OldWords = { "&nbsp;", "&amp;", "&quot;", "&lt;", "&gt;", "&reg;", "&copy;", "&bull;", "&trade;" };
            string[] NewWords = { " ", "&", "\"", "<", ">", "Â®", "Â©", "â€¢", "â„¢" };
            for (int i = 0; i < OldWords.Length; i++)
            {
                sbHTML.Replace(OldWords[i], NewWords[i]);
            }

            // Finally, remove all HTML tags and return plain text
            return Regex.Replace(sbHTML.ToString(), "<[^>]*>", "");
        }

        /// <summary>
        /// To convert case as swagger may be using lower camel case
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string ToPascalCase(this string inputString)
        {
            // If there are 0 or 1 characters, just return the string.
            if (inputString == null) return null;
            if (inputString.Length < 2) return inputString.ToUpper();
            return inputString.Substring(0, 1).ToUpper() + inputString.Substring(1);
        }

        public static bool Contains(this string source, string toCheck, StringComparison comparison)
        {
            return source?.IndexOf(toCheck, comparison) >= 0;
        }

        public static bool Contains(this string[] strings, string toCheck, StringComparison comparison)
        {
            return strings.Any(x => x.Contains(toCheck, comparison));
        }

        public static void TrimObject(this object obj)
        {
            if (obj == null)
                return;

            var type = obj.GetType();

            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(ø => ø.CanRead && ø.CanWrite).ToList();
            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(string))
                {
                    var propValue = (string)property.GetValue(obj);
                    if (propValue != null)
                        property.SetValue(obj, propValue.Trim());
                }
                else if (property.PropertyType.IsClass)
                {
                    var propObj = property.GetValue(obj);
                    if (propObj != null)
                        propObj.TrimObject();
                }
                else if (property.PropertyType == typeof(IEnumerable))
                {
                    var propList = (IEnumerable)property.GetValue(obj);
                    foreach (var item in propList)
                        item.TrimObject();
                }
            }
        }
    }
}
