using System.Threading.Tasks;

namespace Lotus.Helpers
{
    public static class TaskHelper
    {
        public static void RunSync(this Task task)
        {
            task.GetAwaiter().GetResult();
        }

        public static T RunSync<T>(this Task<T> task)
        {
            return task.GetAwaiter().GetResult();
        }
    }
}
