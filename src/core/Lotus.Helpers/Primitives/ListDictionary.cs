﻿using System.Collections.Generic;

namespace Lotus.Helpers.Primitives
{
    public class ListDictionary<TKey, TListElement> : Dictionary<TKey, IList<TListElement>>
    {
        public void AddElement(TKey key, TListElement element)
        {
            if (ContainsKey(key))
            {
                base[key].Add(element);
            }
            else
            {
                Add(key, new List<TListElement>(new TListElement[] { element }));
            }
        }
    }
}
