using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lotus.Helpers
{
    public static class CollectionHelper
    {
        public static List<object> AsList(this IEnumerable array)
        {
            var result = new List<object>();

            foreach (var item in array)
                result.Add(item);

            return result;
        }

        public static bool IsArray(this object value)
        {
            if (value == null)
                return false;

            if (value is string)
                return false;

            if (value is byte[])
                return false;

            return value is IEnumerable;
        }

        public static void AddRangeOverride<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.For(x => dic[x.Key] = x.Value);
        }

        public static void AddRangeNewOnly<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.For(x => { if (!dic.ContainsKey(x.Key)) dic.Add(x.Key, x.Value); });
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dic, IDictionary<TKey, TValue> dicToAdd)
        {
            dicToAdd.For(x => dic.Add(x.Key, x.Value));
        }

        public static bool ContainsKeys<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<TKey> keys)
        {
            bool result = false;
            keys.ForOrBreak((x) => { result = dic.ContainsKey(x); return result; });
            return result;
        }

        public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue defaultValue = default(TValue))
        {
            dic.TryGetValue(key, out defaultValue);
            return defaultValue;
        }

        public static void For<T>(this IEnumerable<T> items, Action<T> method)
        {
            foreach (var item in items)
                method.Invoke(item);
        }

        /// <summary>
        /// For with Index
        /// </summary>
        public static void For<T>(this IEnumerable<T> items, Action<T, int> method)
        {
            int idx = 0;
            foreach (var item in items)
                method.Invoke(item, idx++);
        }

        public static void ForOrBreak<T>(this IEnumerable<T> source, Func<T, bool> func)
        {
            foreach (var item in source)
            {
                bool result = func(item);
                if (result) break;
            }
        }

        public static void ForOrBreak<T>(this IEnumerable<T> source, Func<T, int, bool> func)
        {
            int idx = 0;
            foreach (var item in source)
            {
                bool result = func(item, idx);
                if (result) break;
            }
        }

        public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T> itemsToRemove)
        {
            if (itemsToRemove == null)
                return;

            foreach (T item in itemsToRemove)
                collection.Remove(item);
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> itemsToAdd)
        {
            if (itemsToAdd == null)
                return;

            foreach (T item in itemsToAdd)
                collection.Add(item);
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            return list == null || !list.Any();
        }
    }
}
