using Lotus.Abstract;
using Lotus.Msm.Events;
using Lotus.Msm.Jobs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Lotus.Msm.Services
{
    /// <summary>
    /// System event service. Adds events to the queue or executes synchronously.
    /// </summary>
    public class SystemEventService
    {
        private readonly SystemEventConfiguration _systemEventConfig;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<SystemEventService> _logger;
        private readonly BackgroundJobService _jobService;

        private readonly Queue<SystemEvent> _eventQueue = new Queue<SystemEvent>();

        public SystemEventService(
            SystemEventConfiguration systemEventConfig,
            IServiceProvider serviceProvider,
            ILogger<SystemEventService> logger,
            BackgroundJobService jobService)
        {
            _systemEventConfig = systemEventConfig;
            _serviceProvider = serviceProvider;
            _logger = logger;
            _jobService = jobService;
        }

        /// <summary>
        /// Adds events to the queue. Sync events will be run at the end of the current context. Async and Background
        /// events will be added to hangfire queue. Events will not be executed if any error occurs in the current context.
        /// </summary>
        public void AddEventToQueue(SystemEvent e)
        {
            if (e == null)
                throw new ArgumentNullException(nameof(e));

            _eventQueue.Enqueue(e);
        }

        /// <summary>
        /// Triggers all events. We used to do that at the end of the request if transaction is succeseded. See references.
        /// </summary>
        /// <returns></returns>
        public async Task ExecuteAllEvents()
        {
            SystemEvent e;

            while (_eventQueue.TryDequeue(out e))
            {
                await TriggerEvent(e);
            }
        }

        /// <summary>
        /// Allows to execute event in the current context. Use AddEventToQueue() if you need to execute sync events at
        /// the end of the current context. Async events will be always added to the queue anyway. In most of the
        /// situations you must use 'AddEventToQueue()' method, unless you want to trigger and execute the event
        /// imidiately (sync).
        /// </summary>
        public async Task TriggerEvent(SystemEvent e, CancellationToken token = default)
        {
            if (e == null)
                throw new ArgumentNullException(nameof(e));

            var eventName = e.GetType().Name;

            var subscribers = _systemEventConfig.GetSubscribers(eventName);
            foreach (var subscriber in subscribers)
            {
                var filterCheck = subscriber.SystemEventAttribute.EventFilter(e);
                if (!filterCheck)
                    continue;

                var delay = subscriber.SystemEventAttribute.DelayMinutes > 0
                    ? (TimeSpan?)TimeSpan.FromMinutes(subscriber.SystemEventAttribute.DelayMinutes)
                    : null;

                switch (subscriber.SystemEventAttribute.EventType)
                {
                    case EventTypeEnum.Sync:
                        _logger.LogDebug($"EventTrigger: '{eventName}', Type: '{subscriber.SystemEventAttribute.EventType}', Service: '{subscriber.Method.DeclaringType.Name}', Operation: '{subscriber.Method.Name}'");
                        await ExecuteSyncronously(subscriber.Method, e, token);
                        break;

                    case EventTypeEnum.AsyncCritical:
                        _logger.LogDebug($"EventTrigger: '{eventName}', Type: '{subscriber.SystemEventAttribute.EventType}', Service: '{subscriber.Method.DeclaringType.Name}', Operation: '{subscriber.Method.Name}'");
                        // Add to hangfire async critical queue
                        _jobService.AddToBackgroundQueue(subscriber.Method, JobQueues.critical, delay, GetArgumentsForMethod(subscriber.Method, e, token));
                        break;

                    case EventTypeEnum.AsyncDefault:
                        _logger.LogDebug($"EventTrigger: '{eventName}', Type: '{subscriber.SystemEventAttribute.EventType}', Service: '{subscriber.Method.DeclaringType.Name}', Operation: '{subscriber.Method.Name}'");
                        // Add to hangfire async default queue
                        _jobService.AddToBackgroundQueue(subscriber.Method, JobQueues.@default, delay, GetArgumentsForMethod(subscriber.Method, e, token));
                        break;

                    case EventTypeEnum.AsyncBackground:
                        _logger.LogDebug($"EventTrigger: '{eventName}', Type: '{subscriber.SystemEventAttribute.EventType}', Service: '{subscriber.Method.DeclaringType.Name}', Operation: '{subscriber.Method.Name}'");
                        // Add to hangfire background queue
                        _jobService.AddToBackgroundQueue(subscriber.Method, JobQueues.background, delay, GetArgumentsForMethod(subscriber.Method, e, token));
                        break;

                    default:
                        throw new NotImplementedException($"The Event type '{subscriber.SystemEventAttribute.EventType}' is not implemented.");
                }
            }
        }

        private static object[] GetArgumentsForMethod(MethodInfo method, SystemEvent e, CancellationToken cancellationToken)
        {
            var methodParams = method.GetParameters();

            object[] methodArgs;

            switch (methodParams.Length)
            {
                case 0:
                    methodArgs = new object[] { };
                    break;
                case 1:
                    methodArgs = methodParams[0].ParameterType == typeof(CancellationToken) ? new object[] { cancellationToken } : new object[] { e };
                    break;
                case 2:
                    methodArgs = new object[] { e, cancellationToken };
                    break;
                default:
                    throw new Exception("The service method has a wrong number of arguments. Must be the expected event provided and following CancellationToken. They both are optional.");
            }

            return methodArgs;
        }

        private async Task ExecuteSyncronously(MethodInfo method, SystemEvent e, CancellationToken cancellationToken)
        {
            var service = _serviceProvider.GetService(method.DeclaringType)
                ?? Activator.CreateInstance(method.DeclaringType)
                ?? throw new Exception($"Provided services not found '{method.DeclaringType.FullName}'.");

            var methodParams = method.GetParameters();

            var methodArgs = GetArgumentsForMethod(method, e, cancellationToken);

            if (method.ReturnType == typeof(Task))
            {
                var task = (Task)method.Invoke(service, methodArgs);
                await task.ConfigureAwait(false);
                await Task.Run(() => task.GetAwaiter(), cancellationToken);
            }
            else
            {
                await Task.Run(() => method.Invoke(service, methodArgs), cancellationToken);
            }

            if (service is ITransientService)
            {
                if (service is IAsyncDisposable)
                {
                    await ((IAsyncDisposable)service).DisposeAsync();
                }
                else if (service is IDisposable)
                {
                    ((IDisposable)service).Dispose();
                }
            }
        }

        public void ClearQueue()
        {
            _eventQueue.Clear();
        }
    }
}
