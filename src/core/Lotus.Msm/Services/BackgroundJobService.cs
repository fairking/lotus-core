using Lotus.Abstract;
using Lotus.Helpers;
using Lotus.Msm.Jobs;
using Hangfire;
using Hangfire.States;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

namespace Lotus.Msm.Services
{
    /// <summary>
    /// Add background jobs to the Hangfire service
    /// </summary>
    public class BackgroundJobService : IDisposable
    {
        private readonly IBackgroundJobClient _jobService;
        private readonly IRecurringJobManager _recurringJobManager;
        private readonly IUserContext _userContext;
        private readonly Queue<JobMeta> _jobQueue = new Queue<JobMeta>();

        public BackgroundJobService(IBackgroundJobClient jobService, IRecurringJobManager recurringJobManager, IUserContext userContext)
        {
            _jobService = jobService;
            _recurringJobManager = recurringJobManager;
            _userContext = userContext;
        }

        public void AddToBackgroundQueue(MethodInfo methodInfo, JobQueues jobQueue, params object[] arguments)
        {
            _jobQueue.Enqueue(new JobMeta(methodInfo, jobQueue, _userContext.UserId, _userContext.TimeZone, _userContext.Culture, arguments));
        }

        public void AddToBackgroundQueue(MethodInfo methodInfo, JobQueues jobQueue, DateTime? scheduledUtc, params object[] arguments)
        {
            _jobQueue.Enqueue(new JobMeta(methodInfo, jobQueue, _userContext.UserId, _userContext.TimeZone, _userContext.Culture, arguments) { ScheduledUtc = scheduledUtc });
        }

        public void AddToBackgroundQueue(MethodInfo methodInfo, JobQueues jobQueue, TimeSpan? delay, params object[] arguments)
        {
            _jobQueue.Enqueue(new JobMeta(methodInfo, jobQueue, _userContext.UserId, _userContext.TimeZone, _userContext.Culture, arguments) { Delay = delay });
        }

        public void AddToBackgroundQueue<T>(Expression<Action<T>> action, JobQueues jobQueue)
        {
            var method = ReflectionHelper.GetMethodInfo(action);
            var parameters = ReflectionHelper.GetMethodParameters(action);
            AddToBackgroundQueue(method, jobQueue, parameters);
        }

        public void AddUpdateRecurringJob(string jobId, MethodInfo methodInfo, string cronExpression)
        {
            if (string.IsNullOrEmpty(cronExpression))
                throw new ArgumentNullException(nameof(cronExpression));

            https://docs.hangfire.io/en/latest/background-methods/using-cancellation-tokens.html
            _recurringJobManager.AddOrUpdate(
                jobId,
                new JobMeta(methodInfo, JobQueues.@default, _userContext.UserId, _userContext.TimeZone, _userContext.Culture, GetArgumentsForMethod(methodInfo, CancellationToken.None)), 
                cronExpression);
        }

        public void RemoveRecurringJob(string jobId)
        {
            _recurringJobManager.RemoveIfExists(jobId);
        }

        public void CommitAllJobs()
        {
            JobMeta job;
            while (_jobQueue.TryDequeue(out job))
            {
                // TODO: Queue name not supported for scheduled for a while https://github.com/HangfireIO/Hangfire/issues/1159
                if (job.ScheduledUtc.HasValue)
                    _jobService.Create(job, new ScheduledState(job.ScheduledUtc.Value));
                else if (job.Delay.HasValue)
                    _jobService.Create(job, new ScheduledState(job.Delay.Value));
                else
                    _jobService.Create(job, new EnqueuedState(job.JobQueue.ToString()));
            }
        }

        public void RejectAllJobs()
        {
            _jobQueue.Clear();
        }

        public void Dispose()
        {
            RejectAllJobs();
        }

        private static object[] GetArgumentsForMethod(MethodInfo method, CancellationToken cancellationToken)
        {
            var methodParams = method.GetParameters();

            object[] methodArgs;

            switch (methodParams.Length)
            {
                case 0:
                    methodArgs = new object[] { };
                    break;
                case 1:
                    methodArgs = methodParams[0].ParameterType == typeof(CancellationToken) ? new object[] { cancellationToken } : new object[] { };
                    break;
                default:
                    throw new Exception("The recurring job service method has a wrong number of arguments. Must be the expected CancellationToken. It is optional.");
            }

            return methodArgs;
        }
    }
}
