﻿using Hangfire.Client;
using System.Diagnostics;

namespace Lotus.Msm.Jobs
{
    /// <summary>
    /// Passing some session related values (UserId, Timezone, etc.) from Job object to the Job Parameter
    /// </summary>
    public class JobClientSessionFilter : IClientFilter
    {
        public JobClientSessionFilter()
        {
        }

        public void OnCreating(CreatingContext filterContext)
        {
            var job = filterContext.Job as JobMeta;
            if (job != null)
            {
                filterContext.SetJobParameter(nameof(JobMeta.UserId), job.UserId);
                filterContext.SetJobParameter(nameof(JobMeta.TimeZone), job.TimeZone);
                filterContext.SetJobParameter(nameof(JobMeta.Culture), job.Culture);
            }
        }

        public void OnCreated(CreatedContext filterContext)
        {
            Debug.WriteLine($"JobCreated: {filterContext.Job.Type.Name}.{filterContext.Job.Method.Name}");
        }

    }
}
