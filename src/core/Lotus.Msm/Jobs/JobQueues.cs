﻿namespace Lotus.Msm.Jobs
{
    public enum JobQueues
    {
        /// <summary>
        /// Default queue for general purpose
        /// </summary>
        @default,
        /// <summary>
        /// Critical queue for critical things which cannot wait. Eg. generating a PDF which is expected by user to download.
        /// </summary>
        critical,
        /// <summary>
        /// Background queue where jobs are insignificant. Eg. sending notification emails.
        /// </summary>
        background,
    }
}
