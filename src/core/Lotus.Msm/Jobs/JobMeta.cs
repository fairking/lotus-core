﻿using Hangfire.Common;
using System;
using System.Reflection;

namespace Lotus.Msm.Jobs
{
    public class JobMeta : Job
    {
        public JobQueues JobQueue { get; protected set; }
        public string UserId { get; protected set; }
        public string TimeZone { get; protected set; }
        public string Culture { get; protected set; }
        public DateTime? ScheduledUtc { get; set; }
        public TimeSpan? Delay { get; set; }

        public JobMeta(MethodInfo method, JobQueues jobQueue, string userId, string timeZone, string culture, params object[] args) : base(method, args)
        {
            JobQueue = jobQueue;
            UserId = userId;
            TimeZone = timeZone;
            Culture = culture;
        }
    }
}
