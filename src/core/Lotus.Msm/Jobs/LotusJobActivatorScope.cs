﻿using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Lotus.Msm.Jobs
{
    public class LotusJobActivatorScope : JobActivatorScope
    {
        private readonly IServiceScope _serviceScope;

        public bool Disposed { get; protected set; }

        public LotusJobActivatorScope(IServiceScope serviceScope)
        {
            _serviceScope = serviceScope ?? throw new ArgumentNullException(nameof(serviceScope));
        }

        public override object Resolve(Type type)
        {
            return ActivatorUtilities.GetServiceOrCreateInstance(_serviceScope.ServiceProvider, type);
        }

        public void DisposeOutside()
        {
            _serviceScope.Dispose();
            base.DisposeScope();
            Disposed = true;
        }

        public override void DisposeScope()
        {
        }
    }
}
