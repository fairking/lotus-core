using Hangfire.Server;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Hangfire;
using System.Data;
using Lotus.Msm.Services;

namespace Lotus.Msm.Jobs
{
    public class JobServerSessionFilter : IServerFilter
    {
        private readonly JobActivator _jobActivator;

        [ThreadStatic]
        private static LotusJobActivatorScope _scope;

        [ThreadStatic]
        private static IDbTransaction _transaction;

        public JobServerSessionFilter(LotusJobActivator jobActivator)
        {
            _jobActivator = jobActivator;
        }

        public void OnPerforming(PerformingContext filterContext)
        {
            _scope = (LotusJobActivatorScope)_jobActivator.BeginScope(filterContext);
            //var connection = (IDbConnection)_scope.Resolve(typeof(IDbConnection));
            //_transaction = connection.BeginTransaction();
        }

        public void OnPerformed(PerformedContext filterContext)
        {
            var bgService = (BackgroundJobService)_scope.Resolve(typeof(BackgroundJobService));

            try
            {
                if (filterContext.Canceled || filterContext.Exception != null)
                {
                    _transaction?.Rollback();

                    HandleException(filterContext);

                    bgService.RejectAllJobs();
                }
                else
                {
                    _transaction?.Commit();

                    bgService.CommitAllJobs();
                }
            }
            catch (Exception exc)
            {
                _transaction?.Rollback();

                bgService.RejectAllJobs();

                throw exc;
            }
            finally
            {
                _transaction?.Dispose();
                _scope.DisposeOutside();
            }
        }

        private void HandleException(PerformedContext filterContext)
        {
            if (filterContext.Exception == null)
                return;

            var loggingService = (ILogger<JobServerSessionFilter>)_scope.Resolve(typeof(ILogger<JobServerSessionFilter>));

            // Log the error.
            loggingService.LogError(filterContext.Exception, $"Error while executing background job '{filterContext.BackgroundJob.Job.Type.FullName}.{filterContext.BackgroundJob.Job.Method.Name}'.");
        }
    }
}
