﻿using Lotus.Abstract;
using Hangfire;
using Hangfire.Server;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Lotus.Msm.Jobs
{
    public class LotusJobActivator : JobActivator
    {
        private readonly IServiceProvider _serviceProvider;

        [ThreadStatic]
        private static JobActivatorScope _scope;

        public LotusJobActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider)); ;
        }

        public override JobActivatorScope BeginScope(JobActivatorContext context)
        {
            if (_scope == null || (_scope as LotusJobActivatorScope).Disposed)
                _scope = CreateScope(
                    context.GetJobParameter<string>(nameof(JobMeta.UserId)),
                    context.GetJobParameter<string>(nameof(JobMeta.TimeZone)),
                    context.GetJobParameter<string>(nameof(JobMeta.Culture))
                    );

            return _scope;
        }

        public override JobActivatorScope BeginScope(PerformContext context)
        {
            if (_scope == null || (_scope as LotusJobActivatorScope).Disposed)
                _scope = CreateScope(
                    context.GetJobParameter<string>(nameof(JobMeta.UserId)),
                    context.GetJobParameter<string>(nameof(JobMeta.TimeZone)),
                    context.GetJobParameter<string>(nameof(JobMeta.Culture))
                    );

            return _scope;
        }

        private JobActivatorScope CreateScope(string userId, string timezone, string culture)
        {
            var scope = _serviceProvider.CreateScope();
            if (userId != null)
            {
                var currentUser = scope.ServiceProvider.GetService<IUserContext>();
                if (currentUser == null)
                    throw new NotSupportedException("The Service Factory must implement CurrentUser as ICurrentUser interface.");
                currentUser.SetUser(userId, timezone, culture);
            }
            return new LotusJobActivatorScope(scope);
        }
    }
}
