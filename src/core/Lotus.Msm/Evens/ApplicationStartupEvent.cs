using System;
using System.ComponentModel;

namespace Lotus.Msm.Events
{
    [Serializable]
    [Description("This event is triggered when the application is started.")]
    public class ApplicationStartupEvent : SystemEvent
    {
    }

    public class ApplicationStartupEventAttribute : SystemEventAttribute
    {
        public ApplicationStartupEventAttribute(EventTypeEnum eventType) : base(nameof(ApplicationStartupEvent), eventType)
        {

        }
    }
}
