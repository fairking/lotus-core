using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Lotus.Helpers.Primitives;
using Lotus.Helpers;

namespace Lotus.Msm.Events
{
    public class SystemEventConfiguration : IDisposable
    {
        private ListDictionary<string, SystemEventSubscriber> _subscribers;
        private List<SystemEventCron> _cronJobs;

        public SystemEventConfiguration()
        {
            _subscribers = new ListDictionary<string, SystemEventSubscriber>();
            _cronJobs = new List<SystemEventCron>();
        }

        public SystemEventConfiguration(IEnumerable<Type> serviceClasses) : this()
        {
            Configure(serviceClasses);
        }

        public void Configure(IEnumerable<Type> serviceClasses)
        {
            foreach (var serviceClass in serviceClasses)
                Configure(serviceClass);
        }

        public void Configure(Type serviceClass)
        {
            var serviceMethods = serviceClass.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            foreach (var serviceMethod in serviceMethods)
            {
                var systemEventAttribute = serviceMethod.GetCustomAttribute<SystemEventAttribute>();
                if (systemEventAttribute != null)
                {
                    _subscribers.AddElement(systemEventAttribute.EventName, 
                        new SystemEventSubscriber(serviceMethod, systemEventAttribute));
                }
                var cronAttribute = serviceMethod.GetCustomAttribute<CronAttribute>();
                if (cronAttribute != null)
                {
                    _cronJobs.Add(new SystemEventCron(serviceClass.Name.TrimEnd("Service", StringComparison.InvariantCultureIgnoreCase), serviceMethod.Name, cronAttribute));
                }
            }
        }

        /// <summary>
        /// Delete all subscribers related to the serviceMethod. And add a new collection base on systemEvents.
        /// </summary>
        public void Configure(MethodInfo serviceMethod, SystemEventAttribute[] systemEvents)
        {
            // Find all existing subscribers related to the current service method.
            var allMethodSubscribers = _subscribers.SelectMany(x => x.Value)
                .Where(x => x.Method == serviceMethod)
                .ToList();

            // Delete all found subscibers
            allMethodSubscribers.ForEach(x => _subscribers[x.SystemEventAttribute.EventName].Remove(x));

            // Create a new subscriber list
            var newSubscribers = systemEvents != null
               ? systemEvents.Select(x => new SystemEventSubscriber(serviceMethod, x)).ToArray()
               : new SystemEventSubscriber[0];

            // Add that list to all subscribers
            newSubscribers.For(x => _subscribers.AddElement(x.SystemEventAttribute.EventName, x));
        }

        public IReadOnlyList<SystemEventSubscriber> GetSubscribers(string eventName)
        {
            IList<SystemEventSubscriber> subscribers;
            if (_subscribers.TryGetValue(eventName, out subscribers))
                return subscribers.ToList();
            else
                return new List<SystemEventSubscriber>();
        }

        public IReadOnlyList<SystemEventCron> GetCronJobs()
        {
            return _cronJobs;
        }

        public void Dispose()
        {
            _subscribers.Clear();
            _cronJobs.Clear();
        }
    }
}
