using System;
using System.Collections.Generic;
using System.Reflection;

namespace Lotus.Msm.Events
{
    public class SystemEventSubscriber
    {
        public SystemEventAttribute SystemEventAttribute { get; protected set; }

        public MethodInfo Method { get; protected set; }

        public SystemEventSubscriber(MethodInfo method, SystemEventAttribute systemEventAttribute)
        {
            Method = method ?? throw new ArgumentNullException(nameof(method));
            SystemEventAttribute = systemEventAttribute ?? throw new ArgumentNullException(nameof(systemEventAttribute));
        }

        public override bool Equals(object obj)
        {
            return obj is SystemEventSubscriber subscriber &&
                   EqualityComparer<SystemEventAttribute>.Default.Equals(SystemEventAttribute, subscriber.SystemEventAttribute) &&
                   EqualityComparer<MethodInfo>.Default.Equals(Method, subscriber.Method);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(SystemEventAttribute, Method);
        }
    }
}
