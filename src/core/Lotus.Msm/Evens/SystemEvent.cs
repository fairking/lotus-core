using System;

namespace Lotus.Msm.Events
{
    [Serializable]
    public abstract class SystemEvent
    {
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class SystemEventAttribute : Attribute
    {
        public string EventName { get; protected set; }
        public EventTypeEnum EventType { get; protected set; }
        public int DelayMinutes { get; set; }

        public SystemEventAttribute(string eventName, EventTypeEnum eventType)
        {
            EventName = eventName;
            EventType = eventType;
        }

        public virtual bool EventFilter(SystemEvent e)
        {
            return true;
        }
    }

    public enum EventTypeEnum
    {
        /// <summary>
        /// Synchronous event happens at the same time when triggered. The job is running in the same transaction of the main thread.
        /// </summary>
        Sync,

        /// <summary>
        /// Asynchronous event executed by separate thread in separate transaction. It is stack to the Critical queue (see JobQueues)
        /// </summary>
        AsyncCritical,

        /// <summary>
        /// Asynchronous event executed by separate thread in separate transaction. It is stack to the Default queue (see JobQueues)
        /// </summary>
        AsyncDefault,

        /// <summary>
        /// Asynchronous event executed by separate thread in separate transaction. It is stack to the Background queue (see JobQueues)
        /// </summary>
        AsyncBackground
    }
}
