using System;

namespace Lotus.Msm.Events
{
    public class SystemEventCron
    {
        public CronAttribute CronAttribute { get; protected set; }
        public string Service { get; protected set; }
        public string Operation { get; protected set; }

        public SystemEventCron(string service, string operation, CronAttribute cronAttribute)
        {
            if (string.IsNullOrEmpty(service))
                throw new ArgumentNullException("service");

            if (string.IsNullOrEmpty(operation))
                throw new ArgumentNullException("operation");

            if (cronAttribute == null)
                throw new ArgumentNullException("cronAttribute");

            Service = service;
            Operation = operation;
            CronAttribute = cronAttribute;
        }
    }

    /// <summary>
    /// Repeats a background job every X period. Remember to add it to appsettings.json -&gt; Jobs.RecurringTasks as
    /// {ServiceName.MethodName}. The service method must have no arguments. Cron expression
    /// https://en.wikipedia.org/wiki/Cron#CRON_expression, eg. "*/15 * * * *" means every 15 minutes.
    /// </summary>
    public class CronAttribute : Attribute
    {
        public string CronExpression { get; protected set; }

        public CronAttribute(string cronExpression)
        {
            CronExpression = cronExpression;
        }
    }
}
