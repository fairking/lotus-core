﻿namespace Lotus.Sql.Tests.Dummy
{
    public class MyEntity
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}
