using Lotus.Sql.Observable;
using Lotus.Sql.Tests.Dummy;
using System;
using Xunit;

namespace Lotus.Sql.Tests.All
{
    public class ObservableInterceptorTests
    {
        [Fact]
        public void interceptor_is_working()
        {
            var obj = new MyEntity()
            {
                Name = "My name",
                Description = "My description"
            };

            var proxy = ObservableProxy.Create(obj);

            Assert.Equal("My name", proxy.Name);
            Assert.False(ObservableProxy.IsChanged(proxy));

            proxy.Name = "My name";

            Assert.Equal("My name", proxy.Name);
            Assert.False(ObservableProxy.IsChanged(proxy));

            proxy.Name = "My name 2";

            Assert.Equal("My name 2", proxy.Name);
            Assert.True(ObservableProxy.IsChanged(proxy));
            Assert.Collection(ObservableProxy.ChangedProperties(proxy), item => Assert.Equal("Name", item));

            ObservableProxy.ClearChanged(proxy);
            Assert.False(ObservableProxy.IsChanged(proxy));
            Assert.Empty(ObservableProxy.ChangedProperties(proxy));

            var obj2 = ObservableProxy.Unproxy(proxy);
            Assert.Throws<ArgumentException>(() => ObservableProxy.IsChanged(obj2));
            Assert.Throws<ArgumentException>(() => ObservableProxy.ChangedProperties(obj2));
            Assert.Throws<ArgumentException>(() => ObservableProxy.ClearChanged(obj2));
        }
    }
}
