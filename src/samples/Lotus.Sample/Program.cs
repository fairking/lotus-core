using Lotus.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Lotus.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args);

            hostBuilder.ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });

            LotusBootstrap.ConfigureHost(hostBuilder, args, typeof(Program).Assembly);

            hostBuilder.Build().Run();
        }
    }
}
