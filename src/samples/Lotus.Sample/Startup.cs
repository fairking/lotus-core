using Lotus.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lotus.Api
{
    public class Startup
    {
        public LotusBootstrap Bootstrap { get; }

        public Startup(IConfiguration configuration)
        {
            Bootstrap = new LotusBootstrap(configuration);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Bootstrap.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Bootstrap.ConfigureApp(app, env);
        }
    }
}
