## Introduction

Lotus Core is a simple lightweight C# web/api framework for various use.

## How to use

Coming soon (currently in development)

## How to contribute

Please create an issue ticket to be a contributer.

## Donations

Donate with [nano](https://nano.org).

[![Lotus Core donations](https://gitlab.com/fairking/lotus-core/-/raw/master/docs/donations_nano.png)](https://nanocrawler.cc/explorer/account/nano_1sygjbkepdcu5diiekf15ar6m6utfgf9rr9tkd6zi8mkq7yza34kiyjpgt9g)
